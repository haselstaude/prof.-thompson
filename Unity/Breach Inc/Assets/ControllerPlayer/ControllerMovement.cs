﻿using UnityEngine;
using System.Collections;
using System;

public class ControllerMovement : MonoBehaviour {

    [SerializeField]
    private float movementSpeed = 5f;
    [SerializeField]
    private float horSensitivity = 2.5f;
    [SerializeField]
    private float verSensitivity = 1.5f;

    public Camera cam;
    private Vector3 lastValidCameraAngle;

    private Rigidbody rb;
    private Vector3 lastValidRBAngle;

	// Use this for initialization
	void Start () {
        this.rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        // Movement / Look Around
        applyMovement();

        // Jump
        if (Input.GetButton("Jump")) {
            jump();
        }
	}

    private void applyMovement() {
        // Walking velocity
        float velZ = Input.GetAxis("Walk");
        float velX = Input.GetAxis("Strafe");

        // Rotation
        float rotY = Input.GetAxis("Look X");
        float rotX = Input.GetAxis("Look Y");

        // Calculating movement-vector
        Vector3 velZVec3 = transform.forward * velZ;
        Vector3 velXVec3 = transform.right * velX;
        Vector3 vel = (velZVec3 + velXVec3).normalized * this.movementSpeed;

        // Setting up rotation-vectors
        Vector3 rotRB = new Vector3(0f, rotY, 0f) * this.horSensitivity;
        Vector3 rotCam = new Vector3(rotX, 0f, 0f) * this.verSensitivity;

        // Apply position
        this.rb.MovePosition(rb.position + vel * Time.deltaTime);

        // Apply rotation
        this.rb.MoveRotation(rb.rotation * Quaternion.Euler(rotRB));
        this.cam.transform.Rotate(rotCam);

        // Prohibit flips
        recoverRotationIfExceeded();
    }

    private void recoverRotationIfExceeded() {
        if(this.cam.transform.eulerAngles.x >= 60f && this.cam.transform.eulerAngles.x < 90f) {
            // Max angle exceeded (looking down)
            this.cam.transform.eulerAngles = this.lastValidCameraAngle;
            this.rb.transform.eulerAngles = this.lastValidRBAngle;
        } else if(this.cam.transform.eulerAngles.x <= 300f && this.cam.transform.eulerAngles.x > 270f) {
            // Min angle exceeded (looking up)
            this.cam.transform.eulerAngles = this.lastValidCameraAngle;
            this.rb.transform.eulerAngles = this.lastValidRBAngle;
        } else {
            // Angle is okay, save it
            this.lastValidCameraAngle = this.cam.transform.eulerAngles;
            this.lastValidRBAngle = this.rb.transform.eulerAngles;
        }
    }

    private void jump() {
        if (this.rb.velocity.y >= -0.00001f && this.rb.velocity.y <= 0.00001f) {
            // Not in air
            this.rb.AddForce(Vector3.up * 20000f);
        }
    }

}
