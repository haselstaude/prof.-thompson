﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

    [SerializeField]
    private int AMOUNT_SLOTS = 4;

    private int selectedSlot = 0;
    private GameObject[] inventory;

    public GameObject slot0;
    public GameObject slot1;
    public GameObject slot2;
    public GameObject slot3;

    // Use this for initialization
    void Start () {
        this.inventory = new GameObject[AMOUNT_SLOTS];
        hideIcon(0);
        hideIcon(1);
        hideIcon(2);
        hideIcon(3);
	}
	
	// Update is called once per frame
	void Update () {
        // scroll right
        if(Input.GetAxis("ScrollInventory") < 0f || Input.GetButtonUp("ScrollInventoryNext")) {
            this.selectedSlot++;
            if (this.selectedSlot > AMOUNT_SLOTS - 1) {
                this.selectedSlot = 0;
            }
        }

        // scroll left
        if(Input.GetAxis("ScrollInventory") > 0f || Input.GetButtonUp("ScrollInventoryPrevious")) {
            this.selectedSlot--;
            if(this.selectedSlot < 0) {
                this.selectedSlot = AMOUNT_SLOTS - 1;
            }
        }
    }

    /// <summary>
    /// Returns the selected item
    /// </summary>
    /// <param name="remove">Should the item be removed?</param>
    /// <returns>Selected item</returns>
    public GameObject getSelectedItem(bool remove = false) {
        GameObject item = this.inventory[selectedSlot];
        if (remove) {
            this.inventory[selectedSlot] = null;
            hideIcon(selectedSlot);
        }
        return item;
    }

    /// <summary>
    /// Adds an item into a free slot of the inventory
    /// </summary>
    /// <param name="obj">Object</param>
    /// <returns>True if insertion was successful, else inventory full</returns>
    public bool addItem(GameObject obj) {
        for (int i = 0; i < AMOUNT_SLOTS; i++) {
            if (this.inventory[i] == null) {
                this.inventory[i] = obj;
                showIcon(i, obj.GetComponent<InventoryIcon>().icon);
                return true;
            }
        }
        return false;
    }

    private void showIcon(int slot, Texture icon) {
        switch(slot) {
            case 0: { this.slot0.GetComponent<RawImage>().texture = icon; this.slot0.SetActive(true); break; }
            case 1: { this.slot1.GetComponent<RawImage>().texture = icon; this.slot1.SetActive(true); break; }
            case 2: { this.slot2.GetComponent<RawImage>().texture = icon; this.slot2.SetActive(true); break; }
            case 3: { this.slot3.GetComponent<RawImage>().texture = icon; this.slot3.SetActive(true); break; }
            default: break; // should not happen
        }
    }

    private void hideIcon(int slot) {
        switch(slot) {
            case 0: { this.slot0.SetActive(false); break; }
            case 1: { this.slot1.SetActive(false); break; }
            case 2: { this.slot2.SetActive(false); break; }
            case 3: { this.slot3.SetActive(false); break; }
            default: break; // should not happen
        }
    }
}
