﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public GameObject deathGUI;
    public GameObject screenDeathReason;
    public GameObject screenManagerTop;
    public GameObject screenManagerBottom;
    public GameObject usbStick;

    public void Die(string yourFriendDotDotDot) {
        // show for controller-player
        this.deathGUI.SetActive(true);
        this.deathGUI.transform.FindChild("Text").GetComponent<Text>().text = "You " + yourFriendDotDotDot;

        // show for VR-player
        this.screenDeathReason.GetComponent<TextMesh>().text = "Your friend " + yourFriendDotDotDot + "!";
        this.screenManagerTop.GetComponent<ScreenManager>().showScreen(ScreenManager.Screens.YOURFRIENDDIED);
        this.screenManagerBottom.GetComponent<ScreenManager>().showScreen(ScreenManager.Screens.YOURFRIENDDIED);

        if(this.usbStick.GetComponent<SaveData>().checkpoint == null) {
            // no checkpoint set
            // restart level after 5 seconds
            Invoke("restartLevel", 5f);
        } else {
            // set empty screens (in case no screens are in recover data set)
            Invoke("showEmptyScreens", 4.9f);
            // recover data and respawn
            Invoke("recoverData", 5f);
            Invoke("hideDeathGUI", 5f);
        }
    }

    private void restartLevel() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }

    private void showEmptyScreens() {
        this.screenManagerTop.GetComponent<ScreenManager>().showScreen(ScreenManager.Screens.EMPTY);
        this.screenManagerBottom.GetComponent<ScreenManager>().showScreen(ScreenManager.Screens.EMPTY);
    }

    private void recoverData() {
        this.usbStick.GetComponent<SaveData>().checkpoint.GetComponent<SimpleCheckpoint>().recoverData();
    }

    private void hideDeathGUI() {
        this.deathGUI.SetActive(false);
    }
}
