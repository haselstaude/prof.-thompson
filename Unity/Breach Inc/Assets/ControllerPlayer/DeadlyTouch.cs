﻿using UnityEngine;
using System.Collections;

public class DeadlyTouch : MonoBehaviour {

    public GameObject usbStick;
    public string YourFriendDotDotDot = "died";

    void OnTriggerStay(Collider other) {
        if(other.gameObject.GetComponent<Inventory>() != null) {
            // Player touched it
            other.gameObject.GetComponent<Player>().Die(this.YourFriendDotDotDot);
        }
    }

}
