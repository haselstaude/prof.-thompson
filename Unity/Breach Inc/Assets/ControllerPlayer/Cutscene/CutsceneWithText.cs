﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CutsceneWithText : Cutscene {

    public GameObject guiTextElement;
    public string textToShow = "%guiText%";

	public override void Show() {
        base.Show();
        this.guiTextElement.GetComponent<Text>().text = this.textToShow;
        this.guiTextElement.SetActive(true);
    }

    public override void Hide() {
        base.Hide();
        this.guiTextElement.SetActive(false);
    }
}
