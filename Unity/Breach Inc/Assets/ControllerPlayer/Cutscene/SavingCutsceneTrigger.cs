﻿using UnityEngine;
using System.Collections;

public class SavingCutsceneTrigger : MonoBehaviour {

    public GameObject cutsceneManager;

	void OnTriggerEnter(Collider other) {
        if (other.gameObject.GetComponent<Player>() != null) {
            // player triggered
            this.cutsceneManager.GetComponent<CutsceneManager>().ShowFor("Saving_USBStick", 4f);
            Invoke("showUSBSlotCutscene", 4f);
            Invoke("disableTrigger", 8f);
        }
    }

    private void showUSBSlotCutscene() {
        this.cutsceneManager.GetComponent<CutsceneManager>().ShowFor("Saving_USBSlot", 4f);
    }

    private void disableTrigger() {
        this.gameObject.SetActive(false);
    }
}
