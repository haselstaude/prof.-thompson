﻿using UnityEngine;
using System.Collections;

public class Cutscene : MonoBehaviour {
    public GameObject cutsceneGuiElement;
    public Camera cutsceneCamera;

    public virtual void Show() {
        this.cutsceneCamera.enabled = true;
        this.cutsceneGuiElement.SetActive(true);
    }

    public void ShowFor(float seconds) {
        Show();
        Invoke("Hide", seconds);
    }

    public virtual void Hide() {
        this.cutsceneCamera.enabled = false;
        this.cutsceneGuiElement.SetActive(false);
    }
}
