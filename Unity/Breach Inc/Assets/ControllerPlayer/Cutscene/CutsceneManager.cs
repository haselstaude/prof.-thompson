﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CutsceneManager : MonoBehaviour {

    private Dictionary<string, Cutscene> cutscenes;

    void Start() {
        this.cutscenes = new Dictionary<string, Cutscene>();
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Cutscene");
        foreach(GameObject obj in objects) {
            this.cutscenes[obj.name] = obj.GetComponent<Cutscene>();
        }
    }

    public void Show(string name) {
        if (this.cutscenes.ContainsKey(name)) {
            this.cutscenes[name].Show();
        } else {
            Debug.Log("Could not find cutscene " + name);
        }
    }

    public void ShowFor(string name, float seconds) {
        if(this.cutscenes.ContainsKey(name)) {
            this.cutscenes[name].ShowFor(seconds);
        } else {
            Debug.Log("Could not find cutscene " + name);
        }
    }

    public void Hide(string name) {
        if(this.cutscenes.ContainsKey(name)) {
            this.cutscenes[name].Hide();
        } else {
            Debug.Log("Could not find cutscene " + name);
        }
    }

}
