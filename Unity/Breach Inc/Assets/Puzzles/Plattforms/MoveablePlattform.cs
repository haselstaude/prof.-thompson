﻿using UnityEngine;
using System.Collections;

public class MoveablePlattform : MonoBehaviour {

    private static float MIN_POS_Y = -0.15f;
    private static float MAX_POS_Y = 0.15f;
    private static float MAX_VEL = 0.1f;

    private float velocity = 0.0f;

    private float positionX;
    private float positionY = 0f;
    private float positionZ;

    // Use this for initialization
    void Start () {
        // saving X/Z position
        this.positionX = transform.localPosition.x;
        this.positionZ = transform.localPosition.z;

        // generating random Y position
        setToRandomPosY();
	}

    void Update() {
        if (this.velocity != 0f) {
            // plattform should move
            this.positionY += this.velocity * Time.deltaTime;
            if (this.positionY > MAX_POS_Y) {
                this.positionY = MAX_POS_Y;
                this.velocity = 0f;
            }
            if (this.positionY < MIN_POS_Y) {
                this.positionY = MIN_POS_Y;
                this.velocity = 0f;
            }
            setPositionToGeometry();
        }
    }

    private void setToRandomPosY() {
        this.positionY = Random.Range(MoveablePlattform.MIN_POS_Y, MoveablePlattform.MAX_POS_Y);
        setPositionToGeometry();
    }

    private void setPositionToGeometry() {
        transform.localPosition = new Vector3(this.positionX, this.positionY, this.positionZ);
    }

    /// <summary>
    /// Sets the velocity factor
    /// </summary>
    /// <param name="factor">Should be between -1 (max speed downwards) and +1 (max speed upwards)</param>
    public void setVelocityFactor(float factor) {
        if (factor > 1f) {
            factor = 1f;
        }
        if (factor < -1f) {
            factor = -1f;
        }
        this.velocity = MoveablePlattform.MAX_VEL * factor;
    }
}
