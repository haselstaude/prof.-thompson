﻿using UnityEngine;
using System.Collections;
using System;

public class MineRoomScreenMovement : MonoBehaviour {

    private const double kX = 1.01103309929789;
    private const double dX = 9.73420962888661;

    private const double kZ = 1.00342741935484;
    private const double dZ = 40.01227620967745;

    private const double globalZ = -23.93;

    private const float rotationOffset = 180f;

    private GameObject player;

	void Start () {
        // link player
        this.player = GameObject.FindGameObjectsWithTag("Player")[0];
    }
	
	void Update () {
        // update position of player in screen
        float posX = Convert.ToSingle(this.player.transform.position.x * kX + dX);
        float posZ = Convert.ToSingle(this.player.transform.position.z * kZ + dZ + globalZ);

        transform.position = new Vector3(posX, 0.1f, posZ);
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, this.player.transform.eulerAngles.y + MineRoomScreenMovement.rotationOffset, transform.eulerAngles.z);
	}
}
