﻿using UnityEngine;
using System.Collections;

public class MineRoomTile : MonoBehaviour {

    public int posX = -1;
    public int posY = -1;
    public bool mined = false;

    public static bool finished = false;

    void OnTriggerStay(Collider other) {
        GameObject controller = GameObject.FindGameObjectsWithTag("MineController")[0];
        if (controller.GetComponent<MineRoomManager>().isArmed()) {
            // Minefield is armed

            if(other.gameObject.GetComponent<Inventory>() != null) {
                // Player on ground

                if (this.mined) {
                    other.gameObject.GetComponent<Player>().Die("exploded");
                } else {
                    if (this.posX == 4 && this.posY == 5) {
                        // goal of the room
                        controller.GetComponent<MineRoomManager>().EndMineRoom();
                        MineRoomTile.finished = true;
                    }
                }

                // Set SafeSpace
                controller.GetComponent<MineRoomManager>().setSafeSpace(this.posX, this.posY);
            }
        } else {
            // Minefield not armed
            if(!MineRoomTile.finished) {
                // First touch in the MineRoom
                if(this.posY < 5) {
                    controller.GetComponent<MineRoomManager>().LockHimIn();
                }
            }
        }
    }

    public int getGridX() {
        return this.posX;
    }

    public int getGridY() {
        return this.posY;
    }

}
