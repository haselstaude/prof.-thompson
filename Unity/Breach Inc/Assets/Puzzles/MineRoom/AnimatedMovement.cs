﻿using UnityEngine;
using System.Collections;

public class AnimatedMovement : MonoBehaviour {

    public float startX = 0f;
    public float endX = 0f;
    public float startY = 0f;
    public float endY = 0f;
    public float startZ = 0f;
    public float endZ = 0f;
    public float timeNeeded = 1f;

    private float timePassed = 0f;
    private Vector3 recentPosition;

    public bool running = false;

	// Use this for initialization
	void Start () {
        this.recentPosition = new Vector3(this.startX, this.startY, this.startZ);
	}
	
	// Update is called once per frame
	void Update () {
	    if (this.running) {
            // should move
            this.timePassed += Time.deltaTime;
            if (this.timePassed <= this.timeNeeded) {
                this.recentPosition.x = (this.endX - this.startX) / this.timeNeeded * this.timePassed + this.startX;
                this.recentPosition.y = (this.endY - this.startY) / this.timeNeeded * this.timePassed + this.startY;
                this.recentPosition.z = (this.endZ - this.startZ) / this.timeNeeded * this.timePassed + this.startZ;
                gameObject.transform.localPosition = this.recentPosition;
            } else {
                // animation over
                this.running = false;
                this.recentPosition.x = this.endX;
                this.recentPosition.y = this.endY;
                this.recentPosition.z = this.endZ;
                gameObject.transform.localPosition = this.recentPosition;
            }
        }
	}

    public void startAnimation() {
        if (!this.running) {
            this.running = true;
            this.recentPosition.x = this.startX;
            this.recentPosition.y = this.startY;
            this.recentPosition.z = this.startZ;
            this.timePassed = 0f;
        }
    }

    public void setStartX(float startX) {
        this.startX = startX;
    }

    public void setStartY(float startY) {
        this.startY = startY;
    }

    public void setStartZ(float startZ) {
        this.startZ = startZ;
    }

    public void setEndX(float endX) {
        this.endX = endX;
    }

    public void setEndY(float endY) {
        this.endY = endY;
    }

    public void setEndZ(float endZ) {
        this.endZ = endZ;
    }

    public void setTimeNeeded(float timeNeeded) {
        if (timeNeeded > 0f) {
            this.timeNeeded = timeNeeded;
        }
    }
}
