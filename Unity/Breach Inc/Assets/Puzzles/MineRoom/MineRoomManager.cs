﻿using UnityEngine;
using System.Collections;
using Assets.BasicElements;
using System;

public class MineRoomManager : MonoBehaviour {

    [Header("Behaviour", order = 0)]
    private GameObject[,] tiles;
    private bool[,] armedTiles;
    public bool armed = false;

    private IntPoint safeSpace = new IntPoint(1, 0);
    private int safeSpaceUsed = 0;
    public int safeSpaceLimit = 2;

    [Header("VR Screens", order = 1)]
    public GameObject screenManagerTop;
    public GameObject screenManagerBottom;
    private GameObject[,] screenTiles;
    public GameObject screenTimer;
    private int timer = 21; // must start one higher

    [Header("Stuff when leaving", order = 2)]
    public GameObject rightDrawer;
    
	// Use this for initialization
	void Start () {
        this.tiles = new GameObject[6, 6];
        this.screenTiles = new GameObject[6, 6];
        this.armedTiles = new bool[6, 6];
        linkTiles();
        linkScreenTiles();
	}

    private void linkTiles() {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Mined");
        int x = -1;
        int y = -1;
        foreach (GameObject obj in objects) {
            x = obj.GetComponent<MineRoomTile>().getGridX();
            y = obj.GetComponent<MineRoomTile>().getGridY();
            this.tiles[x, y] = obj;
        }
    }

    private void linkScreenTiles() {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("MinedScreen");
        int x = -1;
        int y = -1;
        foreach(GameObject obj in objects) {
            x = obj.GetComponent<MineRoomTile>().getGridX();
            y = obj.GetComponent<MineRoomTile>().getGridY();
            this.screenTiles[x, y] = obj;
        }
    }

    private void generateState() {
        if(this.armed) {
            System.Random rand = new System.Random();
            bool tileArmed = false;

            // generate
            for(int y = 0; y < 6; y++) {
                for(int x = 0; x < 6; x++) {
                    tileArmed = rand.Next(2) == 1;
                    this.armedTiles[x, y] = tileArmed;
                }
            }

            // safe space
            if(this.safeSpaceUsed < this.safeSpaceLimit) {
                this.armedTiles[this.safeSpace.x, this.safeSpace.y] = false;
                this.safeSpaceUsed++;
            }
        }
    }

    private void generateScreenRendering() {
        if(this.armed) {
            Material safeMat = Resources.Load("MinedGround_Screen_Safe", typeof(Material)) as Material;
            Material minedMat = Resources.Load("MinedGround_Screen_Mined", typeof(Material)) as Material;

            for(int y = 0; y < 6; y++) {
                for(int x = 0; x < 6; x++) {
                    if(this.armedTiles[x, y]) {
                        this.screenTiles[x, y].GetComponent<Renderer>().material = minedMat;
                    } else {
                        this.screenTiles[x, y].GetComponent<Renderer>().material = safeMat;
                    }
                }
            }
        }
    }

    private void setStateForMines() {
        if(this.armed) {
            for(int y = 0; y < 6; y++) {
                for(int x = 0; x < 6; x++) {
                    setTileArmed(x, y, this.armedTiles[x, y]);
                }
            }
        }
    }

    private void refreshTimer() {
        if (this.timer > 1) {
            this.timer--;
        } else {
            this.timer = 20;
        }
        this.screenTimer.GetComponent<TextMesh>().text = this.timer.ToString();
    }

    private void setTileArmed(int x, int y, bool armed) {
        this.tiles[x, y].GetComponent<MineRoomTile>().mined = armed;
    }

    public void LockHimIn() {
        // close the door
        GameObject.Find("Manager").GetComponent<DoorManager>().CloseSmallDoorNr(1);

        // show enter-warnings on screen
        this.screenManagerTop.GetComponent<ScreenManager>().showScreen(ScreenManager.Screens.MINEROOM_ENTERING);
        this.screenManagerBottom.GetComponent<ScreenManager>().showScreen(ScreenManager.Screens.MINEROOM_ENTERING);
    }

    public void StartMineRoom() {
        this.armed = true;

        // create start-zone
        createStartZone();

        // show pre-screens
        this.screenManagerTop.GetComponent<ScreenManager>().showScreen(ScreenManager.Screens.MINEROOM_LOADING);
        this.screenManagerBottom.GetComponent<ScreenManager>().showScreen(ScreenManager.Screens.MINEROOM_LOADING);

        // show screens in 5 seconds
        Invoke("showScreens", 5.0f);

        // call functions repeating
        InvokeRepeating("generateState", 4.9f, 20f);
        InvokeRepeating("refreshTimer", 5.0f, 1.0f);
        InvokeRepeating("generateScreenRendering", 5.0f, 20f);
        InvokeRepeating("setStateForMines", 5.1f, 20f);
    }

    public void EndMineRoom() {
        this.armed = false;

        // disable screens
        this.screenManagerTop.GetComponent<ScreenManager>().hideScreen(ScreenManager.Screens.MINEROOM);
        this.screenManagerBottom.GetComponent<ScreenManager>().hideScreen(ScreenManager.Screens.MINEROOM);

        // unlock drawer
        GameObject.Find("CutsceneManager").GetComponent<CutsceneManager>().ShowFor("MineRoom_UnlockingDrawer", 2f);
        Invoke("unlockDrawer", 0.5f);
    }

    private void createStartZone() {
        bool armed = false;
        for (int x = 0; x < 6; x++) {
            for (int y = 0; y < 6; y++) {
                // set all armed except 0|0, 1|0, 2|0, 0|1, 1|1 and 2|1
                armed = !((x == 0 || y == 0) && (y == 0 || y == 1 || y == 2));
                this.setTileArmed(x, y, armed);
            }
        }
    }

    private void showScreens() {
        this.screenManagerTop.GetComponent<ScreenManager>().showScreen(ScreenManager.Screens.MINEROOM);
        this.screenManagerBottom.GetComponent<ScreenManager>().showScreen(ScreenManager.Screens.MINEROOM);
    }

    public void setSafeSpace(int x, int y) {
        if(this.safeSpace.x != x || this.safeSpace.y != y) {
            this.safeSpaceUsed = 0;
        }
        this.safeSpace.x = x;
        this.safeSpace.y = y;
    }

    public bool isArmed() {
        return this.armed;
    }

    private void unlockDrawer() {
        this.rightDrawer.GetComponent<Drawer_Movement_ElectricalLock>().Unlock();

        // stop all Invokes
        CancelInvoke();
    }

    public void reset() {
        this.armed = false;
        CancelInvoke();
    }

}
