﻿using UnityEngine;
using System.Collections;

public class PoseGame : MonoBehaviour {

    public GameObject head;
    public GameObject leftHand;
    public GameObject rightHand;
    public GameObject leftFoot;
    public GameObject rightFoot;

    private int LENGTH_POSE_SEQUENCE = 4;

    public GameObject screen;
    public Material materialScreenOff;
    public Material materialUnlocked;

    public GameObject doorToUnlock;

    public GameObject successLight1;
    public GameObject successLight2;
    public GameObject successLight3;
    public GameObject successLight4;
    private SuccessLight successLight1_comp;
    private SuccessLight successLight2_comp;
    private SuccessLight successLight3_comp;
    private SuccessLight successLight4_comp;

    public bool powered = false;

    private Pose[] poseSequence;
    private int activePoseIdx = 0;

    private int successfulActed = 0;

	// Use this for initialization
	void Start () {
        // set screen off
        screen.GetComponent<MeshRenderer>().material = this.materialScreenOff;

        // get success-light-components
        this.successLight1_comp = this.successLight1.GetComponent<SuccessLight>();
        this.successLight2_comp = this.successLight2.GetComponent<SuccessLight>();
        this.successLight3_comp = this.successLight3.GetComponent<SuccessLight>();
        this.successLight4_comp = this.successLight4.GetComponent<SuccessLight>();

        loadRandomPoseSequence();
        InvokeRepeating("checkPose", 1.9f, 8.0f);
        InvokeRepeating("nextPose", 2.0f, 8.0f);
    }

    private void loadRandomPoseSequence() {
        // amount
        int amountPosesAvailable = gameObject.transform.FindChild("Poses").transform.childCount;

        // load in
        this.poseSequence = new Pose[this.LENGTH_POSE_SEQUENCE];
        System.Random rand = new System.Random();
        for (int i = 0; i < this.LENGTH_POSE_SEQUENCE; i++) {
            this.poseSequence[i] = gameObject.transform.FindChild("Poses").transform.GetChild(rand.Next(amountPosesAvailable)).GetComponent<Pose>();
        }
    }

    private void showPoseIdx(int idx) {
        screen.GetComponent<MeshRenderer>().material = this.poseSequence[idx].materialToShow;
    }

    private Pose getActivePose() {
        return this.poseSequence[this.activePoseIdx];
    }

    private void nextPose() {
        // next pose
        if (this.activePoseIdx < LENGTH_POSE_SEQUENCE - 1) {
            this.activePoseIdx++;
        } else {
            this.activePoseIdx = 0;
        }

        // show it
        if(powered) {
            showPoseIdx(this.activePoseIdx);
        }
    }

    private void checkPose() {
        if(this.powered) {
            if(this.getActivePose().Check()) {
                this.successfulActed++;
                updateSuccessLights();
                checkUnlock();
            } else {
                bool lostPoints = this.successfulActed > 0;
                this.successfulActed = 0;
                updateSuccessLights(lostPoints);
            }
        }
    }

    private void updateSuccessLights(bool justLost = false) {
        if (justLost) {
            // just lost
            this.successLight1_comp.setFailure();
            this.successLight2_comp.setFailure();
            this.successLight3_comp.setFailure();
            this.successLight4_comp.setFailure();
        } else {
            // set all init
            this.successLight1_comp.setInit();
            this.successLight2_comp.setInit();
            this.successLight3_comp.setInit();
            this.successLight4_comp.setInit();
            // set success for amount
            if (this.successfulActed >= 1) {
                this.successLight1_comp.setSuccess();
            }
            if(this.successfulActed >= 2) {
                this.successLight2_comp.setSuccess();
            }
            if(this.successfulActed >= 3) {
                this.successLight3_comp.setSuccess();
            }
            if(this.successfulActed == 4) {
                // disable the screens
                this.successLight1.SetActive(false);
                this.successLight2.SetActive(false);
                this.successLight3.SetActive(false);
                this.successLight4.SetActive(false);
            }
        }
    }

    private void checkUnlock() {
        if (this.successfulActed >= this.LENGTH_POSE_SEQUENCE) {
            // unlock door
            this.doorToUnlock.GetComponent<Door>().Open();

            // show unlocked screen
            screen.GetComponent<MeshRenderer>().material = this.materialUnlocked;

            // stop InvokeRepeating
            CancelInvoke();
        }
    }
}
