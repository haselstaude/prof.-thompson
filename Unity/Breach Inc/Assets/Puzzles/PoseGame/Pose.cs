﻿using UnityEngine;
using System.Collections;

public class Pose : MonoBehaviour {

    public enum HeadPos {
        Ignored,
        Default,
        Crouched
    }

    public enum HandGesture {
        Ignored,
        Fist,
        FlatHand
    }

    public enum FootPosition {
        Ignored,
        OnGround,
        InAir,
        ToSide,
        ToFront,
        ToBack
    }


    public Material materialToShow;

    public HeadPos targetHeadPos;
    public HandGesture targetLeftHandGest;
    public HandGesture targetRightHandGest;
    public FootPosition targetLeftFootPos;
    public FootPosition targetRightFootPos;

    private GameObject head;
    private GameObject leftHand;
    private GameObject rightHand;
    private GameObject leftFoot;
    private GameObject rightFoot;

	// Use this for initialization
	void Start () {
        // get vr-components from PoseGame
        this.head = gameObject.transform.parent.transform.parent.GetComponent<PoseGame>().head;
        this.leftHand = gameObject.transform.parent.transform.parent.GetComponent<PoseGame>().leftHand;
        this.rightHand = gameObject.transform.parent.transform.parent.GetComponent<PoseGame>().rightHand;
        this.leftFoot = gameObject.transform.parent.transform.parent.GetComponent<PoseGame>().leftFoot;
        this.rightFoot = gameObject.transform.parent.transform.parent.GetComponent<PoseGame>().rightFoot;
    }

    public bool Check() {
        float headX = this.head.transform.position.x;
        float headZ = this.head.transform.position.z;
        return (checkHead() && checkLeftHand() && checkRightHand() && checkLeftFoot(headX, headZ) && checkRightFoot(headX, headZ));
    }

    private bool checkHead() {
        switch(this.targetHeadPos) {
            case HeadPos.Default: return this.head.transform.position.y > 1.3f;
            case HeadPos.Crouched: return this.head.transform.position.y < 1.3f;
            default: return true; // ignored
        }
    }

    private bool checkLeftHand() {
        if(!this.leftHand.activeInHierarchy) {
            return this.targetLeftHandGest == HandGesture.Ignored;
        }

        switch(this.targetLeftHandGest) {
            case HandGesture.Fist: {
                    return this.leftHand.GetComponent<GestureDetector>().isGesturingFist();
                }
            case HandGesture.FlatHand: {
                    return this.leftHand.GetComponent<GestureDetector>().isGesturingFlatHand();
                }
            default: return true; // ignored
        }
    }

    private bool checkRightHand() {
        if(!this.rightHand.activeInHierarchy) {
            return this.targetRightHandGest == HandGesture.Ignored;
        }

        switch(this.targetRightHandGest) {
            case HandGesture.Fist: {
                    return this.rightHand.GetComponent<GestureDetector>().isGesturingFist();
                }
            case HandGesture.FlatHand: {
                    return this.rightHand.GetComponent<GestureDetector>().isGesturingFlatHand();
                }
            default:
                return true; // ignored
        }
    }

    private bool checkLeftFoot(float headX, float headZ) {
        if(!this.leftFoot.activeInHierarchy) {
            return this.targetLeftFootPos == FootPosition.Ignored;
        }

        switch(this.targetLeftFootPos) {
            case FootPosition.InAir: {
                    return this.leftFoot.GetComponent<FootInformation>().isFootRisen();
                }
            case FootPosition.OnGround: {
                    return this.leftFoot.GetComponent<FootInformation>().isFootOnGround();
                }
            case FootPosition.ToSide: {
                    // should be at least 15cm left of head-x | left is -x
                    return headX - this.leftFoot.transform.position.x > 0.15f;
                }
            case FootPosition.ToFront: {
                    // should be at least 10cm in front of head-z | in front is +z
                    return this.leftFoot.transform.position.z - headZ > 0.1f;
                }
            case FootPosition.ToBack: {
                    // should be at least 10cm behind of head-z | in back is -z
                    return headZ - this.leftFoot.transform.position.z > 0.1f;
                }
            default: return true; // ignored
        }
    }

    private bool checkRightFoot(float headX, float headZ) {
        if(!this.rightFoot.activeInHierarchy) {
            return this.targetRightFootPos == FootPosition.Ignored;
        }

        switch(this.targetRightFootPos) {
            case FootPosition.InAir: {
                    return this.rightFoot.GetComponent<FootInformation>().isFootRisen();
                }
            case FootPosition.OnGround: {
                    return this.rightFoot.GetComponent<FootInformation>().isFootOnGround();
                }
            case FootPosition.ToSide: {
                    // should be at least 15cm right of head-x | right is +x
                    return this.rightFoot.transform.position.x - headX > 0.15f;
                }
            case FootPosition.ToFront: {
                    // should be at least 10cm in front of head-z | in front is +z
                    return this.rightFoot.transform.position.z - headZ > 0.1f;
                }
            case FootPosition.ToBack: {
                    // should be at least 10cm behind of head-z | in back is -z
                    return headZ - this.rightFoot.transform.position.z > 0.1f;
                }
            default:
                return true; // ignored
        }
    }
}
