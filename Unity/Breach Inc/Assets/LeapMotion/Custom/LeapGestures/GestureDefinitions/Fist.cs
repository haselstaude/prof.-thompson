﻿using UnityEngine;
using System.Collections;
using System;

public class Fist : ILeapGesture {
    protected float MAX_SINGLE_STRETCH = 0.65f;
    protected float MAX_SUMMARIZED_STRETCH = 1.2f;

    /*
     * This gesture ignores the THUMB, RING- AND INDEX-FINGER.
     * All other fingers must not exceed a stretch-factor of 65%.
     * In total all other fingers must not exceed a summarized stretch-factor of 1.2
     */

    public string getTitle() {
        return "Fist";
    }

    public bool isGestured(LeapHand hand) {
        float index_stretch = hand.getFinger(LeapHand.Finger.INDEX).getStretchFactor();
        if(index_stretch > MAX_SINGLE_STRETCH)
            return false;

        float middle_stretch = hand.getFinger(LeapHand.Finger.MIDDLE).getStretchFactor();
        if(middle_stretch > MAX_SINGLE_STRETCH)
            return false;

        return index_stretch + middle_stretch <= MAX_SUMMARIZED_STRETCH; 
    }
}
