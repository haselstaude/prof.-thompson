﻿using UnityEngine;
using System.Collections;
using System;

public class Pointing : ILeapGesture {
    protected float MIN_INDEX_STRETCH = 0.8f;
    protected float MAX_SINGLE_STRETCH_OTHERS = 0.6f;
    protected float MAX_SUMMARIZED_STRETCH_OTHERS = 1.5f;

    /*
     * This gesture ignores the THUMB.
     * The index finger must exceed a stretch-factor of 80%.
     * All other fingers must not exceed a stretch-factor of 60%.
     * In total all other fingers must not exceed a summarized stretch-factor of 1.5
     */

    public string getTitle() {
        return "Fist";
    }

    public bool isGestured(LeapHand hand) {
        float index_stretch = hand.getFinger(LeapHand.Finger.INDEX).getStretchFactor();
        if(index_stretch < MIN_INDEX_STRETCH)
            return false;

        float middle_stretch = hand.getFinger(LeapHand.Finger.MIDDLE).getStretchFactor();
        if(middle_stretch > MAX_SINGLE_STRETCH_OTHERS)
            return false;

        float ring_stretch = hand.getFinger(LeapHand.Finger.RING).getStretchFactor();
        if(ring_stretch > MAX_SINGLE_STRETCH_OTHERS)
            return false;

        float pinky_stretch = hand.getFinger(LeapHand.Finger.PINKY).getStretchFactor();
        if(pinky_stretch > MAX_SINGLE_STRETCH_OTHERS)
            return false;

        return middle_stretch + ring_stretch + pinky_stretch <= MAX_SUMMARIZED_STRETCH_OTHERS; 
    }
}
