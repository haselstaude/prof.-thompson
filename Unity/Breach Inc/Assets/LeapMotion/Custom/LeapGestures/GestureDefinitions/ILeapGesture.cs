﻿using UnityEngine;
using System.Collections;

public interface ILeapGesture {

    string getTitle();
    bool isGestured(LeapHand hand);

}
