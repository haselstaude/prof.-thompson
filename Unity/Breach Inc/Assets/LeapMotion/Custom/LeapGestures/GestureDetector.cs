﻿using UnityEngine;
using System.Collections;

public class GestureDetector : MonoBehaviour {

    /*
     * Has to be applied on RigidRoundHand_L and RigidRoundHand_R
     */

    public bool showDebugInConsole = true;

    protected LeapHand hand;

    // gestures
    protected Fist gestureFist;
    protected bool gesturingFist = false;

    protected FlatHand gestureFlatHand;
    protected bool gesturingFlatHand = false;

    protected Pointing gesturePointing;
    protected bool gesturingPointing = false;

	
	void Start () {
        this.hand = this.gameObject.GetComponent<LeapHand>();
        if(this.hand == null)
            throw new MissingComponentException(this.gameObject.name + " is missing LeapHand-Script!");

        initGestures();
	}
	
	void Update () {
        if(this.hand.gameObject.activeInHierarchy) {
            checkGestures();
            if(this.showDebugInConsole)
                debugGestures();
        }
	}


    /// <summary>
    /// Initializes gestures -- creates gesture classes
    /// </summary>
    protected void initGestures() {
        this.gestureFist = new Fist();
        this.gestureFlatHand = new FlatHand();
        this.gesturePointing = new Pointing();
    }

    /// <summary>
    /// Checks all learned gestures if gestured
    /// </summary>
    protected void checkGestures() {
        this.gesturingFist = this.gestureFist.isGestured(this.hand);
        this.gesturingFlatHand = this.gestureFlatHand.isGestured(this.hand);
        this.gesturingPointing = this.gesturePointing.isGestured(this.hand);
    }


    public bool isGesturingFist() { return this.gesturingFist; }
    public bool isGesturingFlatHand() { return this.gesturingFlatHand; }
    public bool isGesturingPointing() { return this.gesturingPointing; }


    /// <summary>
    /// Prints which gestures are detected into console
    /// </summary>
    protected void debugGestures() {
        int amountGesturesDetected = 0;


        // gestures
        if(this.isGesturingFist()) {
            Debug.Log(this.gameObject.name + " is gesturing a fist.");
            amountGesturesDetected++;
        }

        if(this.isGesturingFlatHand()) {
            Debug.Log(this.gameObject.name + " is gesturing a flat hand.");
            amountGesturesDetected++;
        }

        if (this.isGesturingPointing()) {
            Debug.Log(this.gameObject.name + " is gesturing pointing.");
            amountGesturesDetected++;
        }


        // warning if more than one gesture detected
        if (amountGesturesDetected > 1) {
            Debug.LogWarning("More than one gesture was detected on " + this.gameObject.name);
        }
    }

}
