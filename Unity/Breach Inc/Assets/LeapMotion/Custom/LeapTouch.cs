﻿using UnityEngine;
using System.Collections;
using Leap.Unity;

public class LeapTouch : MonoBehaviour {

    /*
     * Has to be applied on Objects that should be touchable by Leap
     */

    public GameObject leapLeftHand;
    public GameObject leapRightHand;

    [Tooltip("Factor of the enlargement of the collider-area. 0f would be no enlargement. 1f would lead to double sized collider.")]
    public float colliderEnlargementFactor = 0.2f;

    public bool refreshColliderOnEachTouch = true;

    public Material materialTouched;

    protected Material materialDefault;
    private Bounds boundsCollider;

    protected bool touchingLeft = false;
    protected bool touchingRight = false;

    public bool isTouched() {
        return this.touchingLeft || this.touchingRight;
    }

	
	protected virtual void Start () {
        if(this.gameObject.GetComponent<Renderer>() != null) {
            this.materialDefault = this.gameObject.GetComponent<Renderer>().material;
        }

        // bounds
        this.refreshCollider();
	}

    protected virtual void Update() {
        this.touchingLeft = this.isHandTouching(this.leapLeftHand);
        this.touchingRight = this.isHandTouching(this.leapRightHand);
        if (this.refreshColliderOnEachTouch) {
            refreshCollider();
        }
        this.doTouchColoring();
    }


    protected bool isHandTouching(GameObject hand) {
        bool touching = false;

        // check fingers
        foreach(FingerModel finger in hand.GetComponent<RigidHand>().fingers) {
            touching = this.isFingerTouching(finger.GetTipPosition());
            if (touching)
                return true;
        }

        return false;
    }
    
    protected bool isFingerTouching(Vector3 finger) {
        bool insideX = (this.boundsCollider.min.x < finger.x) && (finger.x < this.boundsCollider.max.x);
        if(!insideX)
            return false;
        bool insideY = (this.boundsCollider.min.y < finger.y) && (finger.y < this.boundsCollider.max.y);
        if(!insideY)
            return false;
        bool insideZ = (this.boundsCollider.min.z < finger.z) && (finger.z < this.boundsCollider.max.z);
        if(!insideZ)
            return false;
        return true;
    }

    protected void doTouchColoring() {
        if(this.gameObject.GetComponent<Renderer>() != null) {
            if(this.touchingLeft || this.touchingRight) {
                this.gameObject.GetComponent<Renderer>().material = this.materialTouched;
            } else {
                this.gameObject.GetComponent<Renderer>().material = this.materialDefault;
            }
        }
    }

    public void refreshCollider() {
        this.boundsCollider = this.gameObject.GetComponent<Collider>().bounds;
        if(this.colliderEnlargementFactor > 0f) {
            // calculate sizes
            float sizeInX = Mathf.Abs(this.boundsCollider.max.x - this.boundsCollider.min.x);
            float sizeInY = Mathf.Abs(this.boundsCollider.max.y - this.boundsCollider.min.y);
            float sizeInZ = Mathf.Abs(this.boundsCollider.max.z - this.boundsCollider.min.z);

            // calculate new bound-minimum
            float minX = this.boundsCollider.min.x - sizeInX * this.colliderEnlargementFactor;
            float minY = this.boundsCollider.min.y - sizeInY * this.colliderEnlargementFactor;
            float minZ = this.boundsCollider.min.z - sizeInZ * this.colliderEnlargementFactor;
            Vector3 newBoundMin = new Vector3(minX, minY, minZ);

            // calculate new bound-maximum
            float maxX = this.boundsCollider.max.x + sizeInX * this.colliderEnlargementFactor;
            float maxY = this.boundsCollider.max.y + sizeInY * this.colliderEnlargementFactor;
            float maxZ = this.boundsCollider.max.z + sizeInZ * this.colliderEnlargementFactor;
            Vector3 newBoundMax = new Vector3(maxX, maxY, maxZ);

            // set new bounds
            this.boundsCollider.SetMinMax(newBoundMin, newBoundMax);
        }
    }
}
