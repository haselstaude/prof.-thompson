﻿using UnityEngine;
using System.Collections;

public class LeapGrab : LeapTouch {

    public Material materialGrabbed;

    protected bool grabbingLeft = false;
    protected bool grabbingRight = false;

    protected Transform origParent;
    public GameObject targetLeftHandJoint;
    public GameObject targetRightHandJoint;


    protected override void Start() {
        base.Start();

        this.origParent = this.gameObject.transform.parent;
    }

    protected override void Update() {
        base.Update();

        // attach layer with grabbed objects to hands if not connection lost
        if(this.leapLeftHand.activeInHierarchy) {
            this.targetLeftHandJoint.transform.localPosition = Vector3.zero;
            this.targetLeftHandJoint.transform.localEulerAngles = Vector3.zero;
        }
        if(this.leapRightHand.activeInHierarchy) {
            this.targetRightHandJoint.transform.localPosition = Vector3.zero;
            this.targetRightHandJoint.transform.localEulerAngles = Vector3.zero;
        }

        if (!this.grabbingLeft && !this.grabbingRight) {
            // not grabbed locally yet

            // check right hand first
            if (this.touchingRight && !this.leapRightHand.GetComponent<LeapHand>().isGrabbing()) {
                if (this.leapRightHand.GetComponent<GestureDetector>().isGesturingFist()) {
                    // touching and fist -> grabbing
                    this.setObjectAsGrabbedWithRightHand();
                }
            }

            if(!this.grabbingRight) {
                // check left hand
                if(this.touchingLeft && !this.leapLeftHand.GetComponent<LeapHand>().isGrabbing()) {
                    if(this.leapLeftHand.GetComponent<GestureDetector>().isGesturingFist()) {
                        // touching and fist -> grabbing
                        this.setObjectAsGrabbedWithLeftHand();
                    }
                }
            }
        } else {
            // already grabbing

            // stick with the grabbing joint
            this.gameObject.transform.localPosition = Vector3.zero;
            this.gameObject.transform.localEulerAngles = Vector3.zero;

            if (this.grabbingRight) {
                // check un-grab
                if(this.leapRightHand.GetComponent<GestureDetector>().isGesturingFlatHand()) {
                    // touching and flat hand -> un-grabbing
                    this.removeObjectFromGrabbingWithRightHand();
                }
            } else {
                // check un-grab
                if(this.leapLeftHand.GetComponent<GestureDetector>().isGesturingFlatHand()) {
                    // touching and flat hand -> un-grabbing
                    this.removeObjecteFromGrabbingWithLeftHand();
                }
            }
        }

        this.doGrabColoring();
    }

    protected void setObjectAsGrabbedWithRightHand() {
        this.grabbingRight = true;
        this.leapRightHand.GetComponent<LeapHand>().setGrabbing(true);
        this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        this.gameObject.GetComponent<Rigidbody>().useGravity = false;
        this.gameObject.transform.SetParent(this.targetRightHandJoint.transform, false);
        this.gameObject.transform.localPosition = Vector3.zero;
        this.gameObject.transform.localEulerAngles = Vector3.zero;
    }

    protected void setObjectAsGrabbedWithLeftHand() {
        this.grabbingLeft = true;
        this.leapLeftHand.GetComponent<LeapHand>().setGrabbing(true);
        this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        this.gameObject.GetComponent<Rigidbody>().useGravity = false;
        this.gameObject.transform.SetParent(this.targetLeftHandJoint.transform, false);
        this.gameObject.transform.localPosition = Vector3.zero;
        this.gameObject.transform.localEulerAngles = Vector3.zero;
    }

    protected void removeObjectFromGrabbingWithRightHand() {
        this.grabbingRight = false;
        this.leapRightHand.GetComponent<LeapHand>().setGrabbing(false);
        this.refreshCollider();
        this.gameObject.GetComponent<Rigidbody>().useGravity = true;
        this.gameObject.transform.SetParent(this.origParent);
    }

    protected void removeObjecteFromGrabbingWithLeftHand() {
        this.grabbingLeft = false;
        this.leapLeftHand.GetComponent<LeapHand>().setGrabbing(false);
        this.refreshCollider();
        this.gameObject.GetComponent<Rigidbody>().useGravity = true;
        this.gameObject.transform.SetParent(this.origParent);
    }


    protected void doGrabColoring() {
        if(this.gameObject.GetComponent<Renderer>() != null) {
            if(this.grabbingLeft || this.grabbingRight) {
                this.gameObject.GetComponent<Renderer>().material = this.materialGrabbed;
            } else {
                if(this.touchingLeft || this.touchingRight) {
                    this.gameObject.GetComponent<Renderer>().material = this.materialTouched;
                } else {
                    this.gameObject.GetComponent<Renderer>().material = this.materialDefault;
                }
            }
        }
    }
}
