﻿using UnityEngine;
using System.Collections;
using Leap.Unity;

public class LeapTouchScreenMaterialChanger : MonoBehaviour {

    protected Bounds touch;
    protected bool currentlyTouched = false;

    public GameObject target;
    public Material materialUntouched;
    public Material materialTouched;

    public GameObject leapLeftHand;
    public GameObject leapRightHand;

    protected virtual void Start() {
        this.touch = this.gameObject.GetComponent<Collider>().bounds;
        this.target.GetComponent<Renderer>().material = this.materialUntouched;
    }

    protected bool isHandTouching(GameObject hand) {
        bool touching = false;

        // check fingers
        foreach(FingerModel finger in hand.GetComponent<RigidHand>().fingers) {
            touching = this.isFingerTouching(finger.GetTipPosition());
            if(touching)
                return true;
        }

        return false;
    }

    protected bool isFingerTouching(Vector3 finger) {
        bool insideX = (this.touch.min.x < finger.x) && (finger.x < this.touch.max.x);
        if(!insideX)
            return false;
        bool insideY = (this.touch.min.y < finger.y) && (finger.y < this.touch.max.y);
        if(!insideY)
            return false;
        bool insideZ = (this.touch.min.z < finger.z) && (finger.z < this.touch.max.z);
        if(!insideZ)
            return false;
        return true;
    }

    void Update() {
        bool touching = this.isHandTouching(this.leapLeftHand);
        if (!touching) {
            touching = this.isHandTouching(this.leapRightHand);
        }

        if (touching && !currentlyTouched) {
            this.target.GetComponent<Renderer>().material = this.materialTouched;
            this.currentlyTouched = true;
        } else if (!touching && currentlyTouched) {
            this.target.GetComponent<Renderer>().material = this.materialUntouched;
            this.currentlyTouched = false;
        }
    }

    protected GameObject isPartOfLeapHand(GameObject obj) {
        while(obj != null) {
            if(obj.GetComponent<LeapHand>() != null) {
                return obj;
            }
            obj = obj.transform.parent.gameObject;
        }
        return null;
    }
}
