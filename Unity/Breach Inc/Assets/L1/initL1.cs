﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class initL1 : MonoBehaviour {

    public GameObject manager;

    // Even early initialization
    void Awake () {
        Cursor.visible = false;
    }

	// Use this for initialization
	void Start () {
        this.manager.GetComponent<DoorManager>().OpenSmallDoorNr(1);
        this.manager.GetComponent<DoorManager>().OpenBigDoorNr(0);
    }

}
