﻿using UnityEngine;
using System.Collections;
using VRTK;

public class PlayIntro : MonoBehaviour {

    [Header("VR", order = 0)]
    public GameObject vrPlayer;
    public GameObject placeToSetVrPlayer;
    public GameObject wholeButtonConstruction;

    [Header("Screen-Player", order = 1)]
    public GameObject guiProLog;
    public Camera guiCamera;

    [Header("Lamp Manager", order = 2)]
    public GameObject manager;
    
    private Vector3 defVrPos;

    private float buttonY = -1.2f;
    private float velY = 0.5f;
    private bool drivingButton = false;

	// Use this for initialization
	void Start () {
        // save origin VR player pos
        this.defVrPos = vrPlayer.transform.position;

        // set VR player in room
        this.vrPlayer.transform.position = placeToSetVrPlayer.transform.position;
        this.vrPlayer.transform.localEulerAngles = new Vector3(0f, 0f, 0f);

        // play intro
        ((MovieTexture)GetComponent<Renderer>().material.mainTexture).anisoLevel = 1;
        ((MovieTexture)GetComponent<Renderer>().material.mainTexture).filterMode = FilterMode.Bilinear;
        ((MovieTexture)GetComponent<Renderer>().material.mainTexture).Play();

        // end of video
        Invoke("driveButton", ((MovieTexture)GetComponent<Renderer>().material.mainTexture).duration - 12f);

        // end for controller player
        Invoke("endForController", ((MovieTexture)GetComponent<Renderer>().material.mainTexture).duration - 10f);
    }

    void driveButton() {
        ((MovieTexture)GetComponent<Renderer>().material.mainTexture).Pause();
        this.drivingButton = true;
    }

    void Update() {
        if (this.drivingButton) {
            this.buttonY += this.velY * Time.deltaTime;
            if (this.buttonY > 0f) {
                this.buttonY = 0f;
                this.drivingButton = false;

                // refresh collider position of button for Leap
                this.wholeButtonConstruction.transform.FindChild("Button").gameObject.GetComponent<LeapTouch>().refreshCollider();
            }
            this.wholeButtonConstruction.transform.localPosition = new Vector3(0f, this.buttonY, 0.7f);
        }
    }

    private void endForController() {
        this.manager.GetComponent<LampManager>().setPowerToGroup(1, true);
        this.guiProLog.SetActive(false);
        this.guiCamera.enabled = false;
    }

    public void end() {
        ((MovieTexture)GetComponent<Renderer>().material.mainTexture).Stop();
        this.vrPlayer.transform.position = this.defVrPos;
        this.vrPlayer.transform.localEulerAngles = new Vector3(0f, 30f, 0f);
        this.gameObject.SetActive(false);
    }
}
