﻿using UnityEngine;
using System.Collections;
using VRTK;

public class GoToControlRoom : MonoBehaviour {

    public GameObject intro;

    void OnTriggerEnter(Collider other) {
        bool endIntro = false;

        // check Vive controller
        // the following is because of heavy parenting in VRTK
        if(other.gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<VRTK_ControllerEvents>() != null) {
            endIntro = true;
        } else {
            // check Leap Motion
            LeapTouch touch = this.gameObject.GetComponent<LeapTouch>();
            if(touch != null) {
                if(touch.isTouched()) {
                    endIntro = true;
                }
            }
        }

        if (endIntro) {
            intro.GetComponent<PlayCalibration>().end();
        }
    }
}
