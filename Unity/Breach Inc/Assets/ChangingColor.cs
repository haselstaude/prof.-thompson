﻿using UnityEngine;
using System.Collections;

public class ChangingColor : MonoBehaviour {

    [Header("Materials", order = 0)]
    public Material mat1;
    public Material mat2;
    public Material mat3;
    public Material mat4;

    [Header("Target", order = 1)]
    public GameObject target;

    [Header("Timing", order = 2)]
    public float timing = 0.5f;

    void Start () {
        InvokeRepeating("setMat1", 0f, this.timing * 4);
        InvokeRepeating("setMat2", this.timing, this.timing * 4);
        InvokeRepeating("setMat3", this.timing * 2, this.timing * 4);
        InvokeRepeating("setMat4", this.timing * 3, this.timing * 4);
    }

    private void setMat(Material mat) {
        this.target.GetComponent<Renderer>().material = mat;
    }

    void setMat1() {
        setMat(mat1);
    }

    void setMat2() {
        setMat(mat2);
    }

    void setMat3() {
        setMat(mat3);
    }

    void setMat4() {
        setMat(mat4);
    }

}
