﻿using UnityEngine;
using System.Collections;

public class PlayCalibration : MonoBehaviour {

    [Header("VR", order = 0)]
    public GameObject doorOpener;

    [Header("Calibration Visuals - Standing", order = 1)]
    public Material calibratingStandingMovie;
    protected bool standingCalibratedBefore = false;

    [Header("Cablibration Visuals - Knee", order = 2)]
    public Material calibratingKneeMovie;
    protected bool kneeCalibratedBefore = false;

    [Header("Calibration Visuals - Walking", order = 3)]
    public Material walkingMovie;

    private float velY = 0.5f;
    private bool openDoor = false;

    // Use this for initialization
    void Start() {

        // start animation
        this.gameObject.GetComponent<Renderer>().material = this.calibratingStandingMovie;
        ((MovieTexture)GetComponent<Renderer>().material.mainTexture).anisoLevel = 1;
        ((MovieTexture)GetComponent<Renderer>().material.mainTexture).filterMode = FilterMode.Bilinear;
        ((MovieTexture)GetComponent<Renderer>().material.mainTexture).loop = true;
        ((MovieTexture)GetComponent<Renderer>().material.mainTexture).Play();

        // start calibration sequence
        this.gameObject.GetComponent<Calibration>().startCalibrating();
    }

    void Update() {
        if (!this.kneeCalibratedBefore) {
            if (!this.standingCalibratedBefore && this.gameObject.GetComponent<Calibration>().isHeightCalibrated()) {
                // height was just calibrated
                this.standingCalibratedBefore = true;
                // stop old visuals
                ((MovieTexture)GetComponent<Renderer>().material.mainTexture).Stop();
                // set new one
                this.gameObject.GetComponent<Renderer>().material = this.calibratingKneeMovie;
                ((MovieTexture)GetComponent<Renderer>().material.mainTexture).anisoLevel = 1;
                ((MovieTexture)GetComponent<Renderer>().material.mainTexture).filterMode = FilterMode.Bilinear;
                ((MovieTexture)GetComponent<Renderer>().material.mainTexture).loop = true;
                ((MovieTexture)GetComponent<Renderer>().material.mainTexture).Play();
            } else if (!this.kneeCalibratedBefore && this.gameObject.GetComponent<Calibration>().isKneeCalibrated()) {
                // knee was just calibrated
                this.kneeCalibratedBefore = true;
                // stop old visuals
                ((MovieTexture)GetComponent<Renderer>().material.mainTexture).Stop();
                // set new one
                this.gameObject.GetComponent<Renderer>().material = this.walkingMovie;
                ((MovieTexture)GetComponent<Renderer>().material.mainTexture).anisoLevel = 1;
                ((MovieTexture)GetComponent<Renderer>().material.mainTexture).filterMode = FilterMode.Bilinear;
                ((MovieTexture)GetComponent<Renderer>().material.mainTexture).loop = true;
                ((MovieTexture)GetComponent<Renderer>().material.mainTexture).Play();
                Invoke("end", 20f);
                // open door
                this.openDoor = true;
            }
        } else if(this.openDoor) {
            float posY = this.doorOpener.transform.localPosition.y;
            posY += this.velY * Time.deltaTime;
            if (posY > 5f) {
                this.openDoor = false;
            }
            this.doorOpener.transform.localPosition = new Vector3(this.doorOpener.transform.localPosition.x, posY, this.doorOpener.transform.localPosition.z);
        }
    }

    public void end() {
        // stop visuals
        ((MovieTexture)GetComponent<Renderer>().material.mainTexture).Stop();

        // stop all
        this.gameObject.SetActive(false);
    }
}
