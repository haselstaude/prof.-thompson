﻿using UnityEngine;
using System.Collections;

public class LampTriggerL1S3 : MonoBehaviour {

    public GameObject lampManager;

    void OnTriggerEnter(Collider other) {
        if(other.gameObject.GetComponent<Player>() != null) {
            // player triggered
            this.lampManager.GetComponent<LampManager>().setPowerToGroup(2, false);
            this.lampManager.GetComponent<LampManager>().setPowerToGroup(3, true);
        }

        // disable trigger
        this.gameObject.SetActive(false);
    }
}