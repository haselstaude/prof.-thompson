﻿using UnityEngine;
using System.Collections;

public class Lamp : MonoBehaviour {

    [Header("Components", order = 0)]
    public GameObject glassComponent;
    public GameObject lightComponent;

    [Header("Materials", order = 1)]
    public Material glassWhenLightOn;
    public Material glassWhenLightOff;

    [Header("Group", order = 2)]
    public int groupNr = 0;

    [Header("Status", order = 3)]
    public bool powerOn = false;

	// Use this for initialization
	void Start () {
	    if (this.powerOn) {
            this.turnOn();
        } else {
            this.turnOff();
        }
	}
	
	public void turnOn() {
        this.powerOn = true;
        this.glassComponent.GetComponent<Renderer>().material = this.glassWhenLightOn;
        this.lightComponent.SetActive(true);
    }

    public void turnOff() {
        this.powerOn = false;
        this.glassComponent.GetComponent<Renderer>().material = this.glassWhenLightOff;
        this.lightComponent.SetActive(false);
    }
}
