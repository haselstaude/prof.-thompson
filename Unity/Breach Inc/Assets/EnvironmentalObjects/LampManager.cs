﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LampManager : MonoBehaviour {

    private List<GameObject> lamps;

    void Start() {
        this.lamps = new List<GameObject>();

        GameObject[] objs = GameObject.FindGameObjectsWithTag("Lamp");
        foreach (GameObject obj in objs) {
            this.lamps.Add(obj);
        }

        this.setPowerToGroup(1, true);
    }

    public void setPowerToGroup(int groupNr, bool power) {
        foreach (GameObject obj in this.lamps) {
            if (obj.GetComponent<Lamp>().groupNr == groupNr) {
                if (power) {
                    obj.GetComponent<Lamp>().turnOn();
                } else {
                    obj.GetComponent<Lamp>().turnOff();
                }
            }
        }
    }
}
