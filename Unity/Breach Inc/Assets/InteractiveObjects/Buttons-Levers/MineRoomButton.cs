﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MineRoomButton : MonoBehaviour {

    [Header("Objects", order = 0)]
    public GameObject mineRoom;
    private MineRoomManager mineRoomManager;

    private GameObject textGUI;

    public bool pressed = false;

    // Use this for initialization
    void Start() {
        this.mineRoomManager = this.mineRoom.GetComponent<MineRoomManager>();

        // get text-element of GUI
        this.textGUI = GameObject.Find("GUI").transform.FindChild("Text").gameObject;
    }

    void OnTriggerStay(Collider other) {
        if(other.gameObject.GetComponent<Inventory>() != null) {
            // Player near Button

            if(!this.pressed) {
                // not activated yet

                // GUI
                this.textGUI.SetActive(true);
                this.textGUI.GetComponent<Text>().text = "Press X to press";

                if(Input.GetButtonUp("Interact")) {
                    // Player pressed Interact-Button
                    gameObject.GetComponent<Animator>().Play("Button_Armature|Button_Pressed");
                    this.pressed = true;

                    // GUI
                    this.textGUI.SetActive(false);

                    // Arm MineRoom
                    this.mineRoomManager.StartMineRoom();

                    openDoor();

                    // show cutscene
                    GameObject.Find("CutsceneManager").GetComponent<CutsceneManager>().ShowFor("MineRoom_OpeningDoor", 2f);
                }
            }
        }
    }

    void OnTriggerExit(Collider other) {
        if(other.gameObject.GetComponent<Inventory>() != null) {
            // Player steps away from Button
            this.textGUI.SetActive(false);
        }
    }

    private void openDoor() {
        GameObject.Find("Manager").GetComponent<DoorManager>().OpenSmallDoorNr(1);
    }
}
