﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

    public int id = -1;
    public bool isBig = false;

    private bool open = false;

    private Animator animator;

    private float openingTimeSmallDoor = 1f;
    private float openingTimeBigDoor = 3f;

	// Use this for initialization
	void Start () {
        this.animator = GetComponent<Animator>();
    }
	
    public void Open() {
        if (!open) {
            if (!isBig) {
                this.animator.Play("Armature_Door|Door_Small_Opening");
                Invoke("disableCollider", this.openingTimeSmallDoor);
            } else {
                this.animator.Play("Armature_Door|Opening");
                Invoke("disableCollider", this.openingTimeBigDoor);
            }
            
            this.open = true;
        }
    }

    public void Close() {
        if (open) {
            if (!isBig) {
                this.animator.Play("Armature_Door|Door_Small_Closing");
            } else {
                this.animator.Play("Armature_Door|Closing");
            }
       
            this.open = false;
            GetComponent<BoxCollider>().enabled = true;
        }
    }

    private void disableCollider() {
        GetComponent<BoxCollider>().enabled = false;
    }
}
