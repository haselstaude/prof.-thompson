﻿using UnityEngine;
using System.Collections;

public class DoorManager : MonoBehaviour {

    public int AMOUNT_SMALL_DOORS = -1;
    public int AMOUNT_BIG_DOORS = -1;

    private GameObject[] doors_small;
    private GameObject[] doors_big;

	// Use this for initialization
	void Start () {
        this.doors_small = new GameObject[this.AMOUNT_SMALL_DOORS];
        this.doors_big = new GameObject[this.AMOUNT_BIG_DOORS];
        loadAllSmallDoors();
        loadAllBigDoors();
	}

    private void loadAllSmallDoors() {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Door_Small");
        foreach(GameObject obj in objects) {
            this.doors_small[obj.GetComponent<Door>().id] = obj;
        }
    }

    private void loadAllBigDoors() {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Door_Big");
        foreach(GameObject obj in objects) {
            this.doors_big[obj.GetComponent<Door>().id] = obj;
        }
    }

    public void OpenSmallDoorNr(int idx) {
        if (idx < this.AMOUNT_SMALL_DOORS) {
            this.doors_small[idx].GetComponent<Door>().Open();
        }
    }

    public void CloseSmallDoorNr(int idx) {
        if(idx < this.AMOUNT_SMALL_DOORS) {
            this.doors_small[idx].GetComponent<Door>().Close();
        }
    }

    public void OpenBigDoorNr(int idx) {
        if(idx < this.AMOUNT_BIG_DOORS) {
            this.doors_big[idx].GetComponent<Door>().Open();
        }
    }

    public void CloseBigDoorNr(int idx) {
        if(idx < this.AMOUNT_BIG_DOORS) {
            this.doors_big[idx].GetComponent<Door>().Close();
        }
    }
}
