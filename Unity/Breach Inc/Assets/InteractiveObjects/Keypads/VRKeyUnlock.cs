﻿using UnityEngine;
using System.Collections;
using VRTK.GrabAttachMechanics;

public class VRKeyUnlock : MonoBehaviour {

    public GameObject key;
    public Material keyOriginalMaterial;
    public GameObject toUnlock0;
    public GameObject toUnlock1;

    private bool locked = true;

    private bool animate = false;
    private float lastAngle = 0f;
    private float animationSpeed = 90f;
    private float animationStop = 140f;

    void Update() {
        if (this.animate) {
            float turned = this.lastAngle + this.animationSpeed * Time.deltaTime;
            if (turned > this.animationStop) {
                turned = this.animationStop;
            }
            this.key.transform.localEulerAngles = new Vector3(this.key.transform.localEulerAngles.x, this.key.transform.localEulerAngles.y, turned);
            this.lastAngle = turned;
            if (turned == this.animationStop) {
                // stop animation
                this.animate = false;

                // disable key
                this.key.SetActive(false);

                // disable lock
                this.gameObject.SetActive(false);
            }
        }
    }

    public void OnTriggerStay(Collider other) {
        if(this.locked) {
            // still locked
            if(other.gameObject.GetComponent<UnlockID>() != null) {
                // is unlockable
                if(other.gameObject.GetComponent<UnlockID>().getID() == this.key.GetComponent<UnlockID>().getID()) {
                    // right unlockable

                    // drop it
                    other.gameObject.GetComponent<Custom_GrabbableObject>().ForceStopInteracting();
                    other.gameObject.GetComponent<Custom_GrabbableObject>().ToggleHighlight(false);

                    // unlock and do visuals
                    this.locked = false;
                    disableKeyScripts();
                    doVisuals();
                    doUnlock();
                }
            }
        }
    }

    private void disableKeyScripts() {
        // Disable Collider
        this.key.GetComponent<BoxCollider>().enabled = false;
        // Disable Rigidbody-Gravity
        this.key.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);
        this.key.GetComponent<Rigidbody>().isKinematic = true;
        // Disable Grabbable and re-color
        this.key.GetComponent<Custom_GrabbableObject>().ToggleHighlight(false);
        this.key.GetComponent<Custom_GrabbableObject>().enabled = false;
        this.key.GetComponent<VRTK_FixedJointGrabAttach>().enabled = false;
    }

    private void doVisuals() {
        this.key.transform.SetParent(this.gameObject.transform);
        this.key.transform.localPosition = new Vector3(-0.45f, 0.28f, 0f);
        this.key.transform.localEulerAngles = new Vector3(0f, -90f, 0f);
        this.animate = true;
    }

    private void doUnlock() {
        this.toUnlock0.GetComponent<Cabinet_KeyLock_Opening>().locked = false;
        this.toUnlock1.GetComponent<Cabinet_KeyLock_Opening>().locked = false;
    }
}
