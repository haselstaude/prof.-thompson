﻿using UnityEngine;
using System.Collections;

public class UnlockID : MonoBehaviour {

    private int id = -1;

	// Use this for initialization
	void Start () {
        System.Random rand = new System.Random(GetInstanceID());
        this.id = rand.Next(1000000000) + 5000000;
	}

    public int getID() {
        return this.id;
    }
}
