﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UnlockControl : MonoBehaviour {

    [Header("Objects", order = 0)]
    public GameObject unlockElement;
    public GameObject controlElement;
    public GameObject screenOptional;

    [Header("Screen Material", order = 1)]
    public Material lockedMaterial;
    public Material unlockedMaterial;

    private bool unlocked = false;

    private GameObject textGUI;
    private bool unlockFail = false;

    // Use this for initialization
    void Start() {
        // set locked material
        if (this.screenOptional != null) {
            this.screenOptional.GetComponent<Renderer>().material = this.lockedMaterial;
        }

        // get text-element of GUI
        this.textGUI = GameObject.Find("GUI").transform.FindChild("Text").gameObject;
    }

    void OnTriggerStay(Collider other) {
        if(other.gameObject.GetComponent<Inventory>() != null) {
            // Player near Control

            if(!this.unlocked && !this.unlockFail) {
                // is locked

                // GUI
                if(other.gameObject.GetComponent<Inventory>().getSelectedItem() != null) {
                    // Player has item in hand
                    this.textGUI.SetActive(true);
                    this.textGUI.GetComponent<Text>().text = "Press X to unlock";
                }

                if(Input.GetButtonUp("Interact")) {
                    // get inventory item
                    GameObject inventoryItem = other.gameObject.GetComponent<Inventory>().getSelectedItem();
                    if (inventoryItem != null) {
                        UnlockID unlockComp = inventoryItem.GetComponent<UnlockID>();
                        if(unlockComp != null) {
                            // is unlockable
                            if(unlockComp.getID() == unlockElement.GetComponent<UnlockID>().getID()) {
                                // id is correct
                                doUnlock();
                                // remove item
                                other.gameObject.GetComponent<Inventory>().getSelectedItem(true);
                                // hide GUI text
                                this.textGUI.SetActive(false);
                            }
                        }
                    }
                    if(!this.unlocked && inventoryItem != null) {
                        // Did not unlock
                        this.textGUI.SetActive(true);
                        this.textGUI.GetComponent<Text>().text = "This won't unlock it";
                        this.unlockFail = true;
                    }
                }
            }
        }
    }

    void OnTriggerExit(Collider other) {
        if(other.gameObject.GetComponent<Inventory>() != null) {
            // Player steps away from Control
            this.textGUI.SetActive(false);
            this.unlockFail = false;
        }
    }

    public void doUnlock() {
        this.unlocked = true;
        if(this.screenOptional != null) {
            this.screenOptional.GetComponent<Renderer>().material = this.unlockedMaterial;
        }
        if (this.controlElement.GetComponent<Door>() != null) {
            // is door
            this.controlElement.GetComponent<Door>().Open();
        }
        // TODO: (other types of controlables)
    }

    public bool isUnlocked() {
        return this.unlocked;
    }
}
