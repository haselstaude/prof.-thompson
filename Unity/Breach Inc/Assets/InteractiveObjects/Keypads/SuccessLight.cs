﻿using UnityEngine;
using System.Collections;

public class SuccessLight : MonoBehaviour {

    public Material initMaterial;
    public Material successMaterial;
    public Material failureMaterial;

    private MeshRenderer meshRenderer;

	// Use this for initialization
	void Start () {
        this.meshRenderer = this.gameObject.GetComponent<MeshRenderer>();
        setInit();
	}

    public void setInit() {
        this.meshRenderer.material = initMaterial;
    }

    public void setFailure() {
        this.meshRenderer.material = failureMaterial;
    }

    public void setSuccess() {
        this.meshRenderer.material = successMaterial;
    }
}
