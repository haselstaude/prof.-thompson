﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Takeable : MonoBehaviour {

    public string objectName = "%missingName%";
    private GameObject textGUI;

	// Use this for initialization
	void Start () {
        // get text-element of GUI
        this.textGUI = GameObject.Find("GUI").transform.FindChild("Text").gameObject;
	}

    void OnTriggerEnter(Collider other) {
        if(other.gameObject.GetComponent<Inventory>() != null) {
            // Player steps to Takeable
            this.textGUI.SetActive(true);
            this.textGUI.GetComponent<Text>().text = "Press B to take " + this.objectName;
        }
    }

    void OnTriggerStay(Collider other) {
        if(other.gameObject.GetComponent<Inventory>() != null) {
            // Player near Takeable

            if(Input.GetButtonUp("InteractBack")) {
                // add to inventory
                Inventory inventory = other.gameObject.GetComponent<Inventory>();
                if (inventory.addItem(gameObject)) {
                    // could be added
                    gameObject.SetActive(false);
                    this.textGUI.SetActive(false);
                } else {
                    this.textGUI.GetComponent<Text>().text = "No space in inventory left";
                }
            }
        }
    }

    void OnTriggerExit(Collider other) {
        if(other.gameObject.GetComponent<Inventory>() != null) {
            // Player steps away from Takeable
            this.textGUI.SetActive(false);
        }
    }
}
