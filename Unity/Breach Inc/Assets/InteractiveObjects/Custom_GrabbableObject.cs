﻿using UnityEngine;
using System.Collections;
using VRTK;

public class Custom_GrabbableObject : VRTK_InteractableObject {

    [Header("Custom Settings", order = 4)]
    public bool hideControllerOnGrab = true;

    private bool grabbed = false;

	// Use this for initialization
	void Start () {
        this.isGrabbable = true;
        this.holdButtonToGrab = true;
	}
	
	public override void Grabbed(GameObject currentGrabbingObject) {
        base.Grabbed(currentGrabbingObject);
        this.grabbed = true;
        
        if (this.hideControllerOnGrab) {
            GameObject controllerModel = currentGrabbingObject.transform.GetChild(0).gameObject;
            for(int i = 0; i < controllerModel.transform.childCount; i++) {
                if(currentGrabbingObject.transform.GetChild(0).gameObject.transform.GetChild(i).GetComponent<MeshRenderer>() != null) {
                    currentGrabbingObject.transform.GetChild(0).gameObject.transform.GetChild(i).GetComponent<MeshRenderer>().enabled = false;
                }
            }
        }
    }

    public override void Ungrabbed(GameObject previousGrabbingObject) {
        base.Ungrabbed(previousGrabbingObject);
        this.grabbed = false;

        if(this.hideControllerOnGrab) {
            GameObject controllerModel = previousGrabbingObject.transform.GetChild(0).gameObject;
            for(int i = 0; i < controllerModel.transform.childCount; i++) {
                if(previousGrabbingObject.transform.GetChild(0).gameObject.transform.GetChild(i).GetComponent<MeshRenderer>() != null) {
                    previousGrabbingObject.transform.GetChild(0).gameObject.transform.GetChild(i).GetComponent<MeshRenderer>().enabled = true;
                }
            }
        }
    }

    public bool isGrabbed() {
        return this.grabbed;
    }

}
