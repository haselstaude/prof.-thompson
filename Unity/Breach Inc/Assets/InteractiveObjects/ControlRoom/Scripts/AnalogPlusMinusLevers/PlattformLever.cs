﻿using UnityEngine;
using System.Collections;

public class PlattformLever : RotatingAnalogPlusMinusLever {

    [Header("Target", order = 4)]
    public GameObject targetPlatform;

    [Header("Complexity", order = 5)]
    public bool invertedValue = false;
    public GameObject additionalPlatformToMove;

    public override void setValueToTarget() {
        float velFactor = this.value;
        if (this.invertedValue) {
            velFactor *= (-1f);
        }
        this.targetPlatform.GetComponent<MoveablePlattform>().setVelocityFactor(velFactor);

        if (this.additionalPlatformToMove != null) {
            this.additionalPlatformToMove.GetComponent<MoveablePlattform>().setVelocityFactor(velFactor);
        }
    }
}
