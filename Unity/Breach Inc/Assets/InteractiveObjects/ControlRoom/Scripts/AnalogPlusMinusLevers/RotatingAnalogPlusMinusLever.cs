﻿using UnityEngine;
using System.Collections;
using VRTK;

public abstract class RotatingAnalogPlusMinusLever : MonoBehaviour {

    public enum Axis {
        X,
        Y,
        Z
    }

    [Header("Materials", order = 0)]
    public Material originalMaterial;
    public Material onHoverMaterial;
    public Material grabMaterial;

    [Header("Movement", order = 1)]
    public Axis axisGrab;
    public bool invertedAxisGrab;
    public float translationFactor = 90f;

    [Header("Values", order = 2)]
    public Axis axisRotation;
    public float neutralAngle;
    public float plusOneAngle;
    public float minusOneAngle;
    private float recentState_lever = 0f;

    [Header("Limits", order = 3)]
    public float minAngle;
    public float maxAngle;

    protected float value = 0f;
    private bool changing = false;

    private bool recentState_grabbed = false;
    private bool lastState_grabbed = false;

    private float grabbedAt_grabObject = 0f;
    private GameObject grabObject;

    // Use this for initialization
    void Start() {
        setNeutral();
	}

    // Update is called once per frame
    void Update() {
        if(this.changing) {
            if(!this.grabObject.GetComponent<GestureDetector>().isGesturingFlatHand()) {
                // still grabbing
                float diffGrabObject = 0f;
                float newValueRotation = 0f;

                // calculating difference of grab
                switch(this.axisGrab) {
                    case Axis.X: { diffGrabObject = this.grabObject.transform.position.x - this.grabbedAt_grabObject; break; }
                    case Axis.Y: { diffGrabObject = this.grabObject.transform.position.y - this.grabbedAt_grabObject; break; }
                    case Axis.Z: { diffGrabObject = this.grabObject.transform.position.z - this.grabbedAt_grabObject; break; }
                    default:
                        break; // cannot happen
                }

                // calculating new position of drawer
                if(this.invertedAxisGrab) {
                    newValueRotation = this.recentState_lever - diffGrabObject * this.translationFactor;
                } else {
                    newValueRotation = this.recentState_lever + diffGrabObject * this.translationFactor;
                }
                if(this.minAngle < 10f) {
                    if(newValueRotation < 360f && newValueRotation > 340f || newValueRotation < this.minAngle) {
                        newValueRotation = this.minAngle;
                    }
                } else {
                    if(newValueRotation < this.minAngle) {
                        newValueRotation = this.minAngle;
                    }
                }
                if(newValueRotation > this.maxAngle) {
                    newValueRotation = this.maxAngle;
                }

                setValueAsAngle(newValueRotation);

                setValueToTarget();

                // setting lever position
                switch(this.axisRotation) {
                    case Axis.X: { this.gameObject.transform.localEulerAngles = new Vector3(newValueRotation, this.gameObject.transform.localEulerAngles.y, this.gameObject.transform.localEulerAngles.z); break; }
                    case Axis.Y: { this.gameObject.transform.localEulerAngles = new Vector3(this.gameObject.transform.localEulerAngles.x, newValueRotation, this.gameObject.transform.localEulerAngles.z); break; }
                    case Axis.Z: { this.gameObject.transform.localEulerAngles = new Vector3(this.gameObject.transform.localEulerAngles.x, this.gameObject.transform.localEulerAngles.y, newValueRotation); break; }
                    default:
                        break; // cannot happen
                }
            } else {
                // stopped grabbing
                this.gameObject.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material = this.originalMaterial;
                this.gameObject.transform.GetChild(1).gameObject.GetComponent<MeshRenderer>().material = this.originalMaterial;
                this.changing = false;
                this.grabObject.GetComponent<LeapHand>().setGrabbing(false);
                this.grabObject = null;

                setNeutral();
                setValueToTarget();

                this.recentState_grabbed = true;
                this.lastState_grabbed = true;
            }
        }
    }

    public void OnTriggerStay(Collider other) {
        if(!this.changing) {
            // not grabbed yet

            // LeapHand touch
            GameObject leapHand = this.isPartOfLeapHand(other.gameObject);
            if(leapHand != null) {
                // hover material
                this.gameObject.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material = this.onHoverMaterial;
                this.gameObject.transform.GetChild(1).gameObject.GetComponent<MeshRenderer>().material = this.onHoverMaterial;

                // check for "new grab"
                this.lastState_grabbed = this.recentState_grabbed;
                this.recentState_grabbed = leapHand.GetComponent<GestureDetector>().isGesturingFist() && !leapHand.GetComponent<LeapHand>().isGrabbing();

                if(!this.lastState_grabbed && this.recentState_grabbed) {
                    // grabbing begins
                    leapHand.GetComponent<LeapHand>().setGrabbing(true);
                    this.changing = true;
                    switch(this.axisGrab) {
                        case Axis.X: { this.grabbedAt_grabObject = leapHand.transform.position.x; break; }
                        case Axis.Y: { this.grabbedAt_grabObject = leapHand.transform.position.y; break; }
                        case Axis.Z: { this.grabbedAt_grabObject = leapHand.transform.position.z; break; }
                        default:
                            break; // cannot happen
                    }
                    this.grabObject = leapHand;

                    // grabbing material
                    this.gameObject.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material = this.grabMaterial;
                    this.gameObject.transform.GetChild(1).gameObject.GetComponent<MeshRenderer>().material = this.grabMaterial;
                }
            }
        }
    }

    public void OnTriggerExit(Collider other) {
        // original material
        this.gameObject.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material = this.originalMaterial;
        this.gameObject.transform.GetChild(1).gameObject.GetComponent<MeshRenderer>().material = this.originalMaterial;

        this.recentState_grabbed = true;
        this.lastState_grabbed = true;
    }

    private void setNeutral() {
        this.value = 0f;
        this.recentState_lever = this.neutralAngle;
        switch(this.axisRotation) {
            case Axis.X: { this.gameObject.transform.localEulerAngles = new Vector3(this.neutralAngle, this.gameObject.transform.localEulerAngles.y, this.gameObject.transform.localEulerAngles.z); break; }
            case Axis.Y: { this.gameObject.transform.localEulerAngles = new Vector3(this.gameObject.transform.localEulerAngles.x, this.neutralAngle, this.gameObject.transform.localEulerAngles.z); break; }
            case Axis.Z: { this.gameObject.transform.localEulerAngles = new Vector3(this.gameObject.transform.localEulerAngles.x, this.gameObject.transform.localEulerAngles.y, this.neutralAngle); break; }
        }
    }

    private void setValueAsAngle(float angle) {
        bool inverted = this.minusOneAngle > this.plusOneAngle;
        if (!inverted) {
            // i.e.
            // -1 > 0 > +1
            // 0° > 90° > 180°
            if(angle >= this.neutralAngle) {
                // in plus range
                this.value = (angle - this.neutralAngle) / (this.plusOneAngle - this.neutralAngle);
                return;
            } else {
                // in minus range
                this.value = (-1f) * (this.neutralAngle - angle) / (this.neutralAngle - this.minusOneAngle);
            }
        } else {
            // i.e.
            // -1 > 0 > +1
            // 180° < 90° < 0°
            if (angle <= this.neutralAngle) {
                // in plus range
                this.value = (this.neutralAngle - angle) / (this.neutralAngle - this.plusOneAngle);
                return;
            } else {
                // in minus range
                this.value = (-1f) * (angle - this.neutralAngle) / (this.minusOneAngle - this.neutralAngle);
                return;
            }
        }
    }

    public virtual void setValueToTarget() { }

    public bool isChanging() {
        return this.changing;
    }

    public float getValue() {
        return this.value;
    }

    protected GameObject isPartOfLeapHand(GameObject obj) {
        while(obj != null) {
            if(obj.GetComponent<LeapHand>() != null) {
                return obj;
            }
            obj = obj.transform.parent.gameObject;
        }
        return null;
    }
}
