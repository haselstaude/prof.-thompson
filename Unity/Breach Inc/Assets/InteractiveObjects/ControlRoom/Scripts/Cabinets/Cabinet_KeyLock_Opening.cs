﻿using UnityEngine;
using System.Collections;

public class Cabinet_KeyLock_Opening : Cabinet_Opening {
    [Header("Locking", order = 4)]
    public bool locked = true;

    public override void OnTriggerStay(Collider other) {
        if (!this.locked) {
            base.OnTriggerStay(other);
        }
    }
}
