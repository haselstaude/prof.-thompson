﻿using UnityEngine;
using System.Collections;
using VRTK;
using System;

public class Cabinet_Opening : MonoBehaviour {

    public enum Axis {
        X,
        Y,
        Z
    }

    [Header("Elements", order = 0)]
    public GameObject door;

    [Header("Materials", order = 1)]
    public Material originalMaterial;
    public Material onHoverMaterial;
    public Material grabMaterial;
    public GameObject materialTargetGameObject;

    [Header("Axis Controller", order = 2)]
    public Axis axisPullStart;
    public Axis axisPullEnd;
    public bool invertedAxisPullStart;
    public bool invertedAxisPullEnd;
    public bool calcDistanceInverted = false;
    public float translationFactor = 1f;

    [Header("Axis Door", order = 3)]
    public Axis axisDoor;
    public float minimumState = 0f;
    public float recentState_door = 0f;
    public float maximumState = 90f;

    private bool recentState_grabbed = false;
    private bool lastState_grabbed = false;
    private bool grabbed = false;
    private float grabbedAt_grabObjectAxisStart= 0f;
    private float grabbedAt_grabObjectAxisEnd = 0f;
    private GameObject grabObject;

    // Update is called once per frame
    void Update() {
        if(this.grabbed) {
            if(!this.grabObject.GetComponent<GestureDetector>().isGesturingFlatHand()) {
                // still grabbing
                float diffGrabObjectAxisPullStart = 0f;
                float diffGrabObjectAxisPullEnd = 0f;
                float totaldiffGrabDiagonal = 0f;
                float newValueRotation = 0f;

                // calculating difference of grab
                switch(this.axisPullStart) {
                    case Axis.X: { diffGrabObjectAxisPullStart = this.grabObject.transform.position.x - this.grabbedAt_grabObjectAxisStart; break; }
                    case Axis.Y: { diffGrabObjectAxisPullStart = this.grabObject.transform.position.y - this.grabbedAt_grabObjectAxisStart; break; }
                    case Axis.Z: { diffGrabObjectAxisPullStart = this.grabObject.transform.position.z - this.grabbedAt_grabObjectAxisStart; break; }
                    default: break; // cannot happen
                }
                switch(this.axisPullEnd) {
                    case Axis.X: { diffGrabObjectAxisPullEnd = this.grabObject.transform.position.x - this.grabbedAt_grabObjectAxisEnd; break; }
                    case Axis.Y: { diffGrabObjectAxisPullEnd = this.grabObject.transform.position.y - this.grabbedAt_grabObjectAxisEnd; break; }
                    case Axis.Z: { diffGrabObjectAxisPullEnd = this.grabObject.transform.position.z - this.grabbedAt_grabObjectAxisEnd; break; }
                    default:
                        break; // cannot happen
                }
                totaldiffGrabDiagonal = Convert.ToSingle(Math.Pow(diffGrabObjectAxisPullStart * diffGrabObjectAxisPullStart + diffGrabObjectAxisPullEnd * diffGrabObjectAxisPullEnd, 1.0 / 2.0));

                // calculating new rotation of door
                if(this.calcDistanceInverted) {
                    newValueRotation = this.recentState_door - diffGrabObjectAxisPullStart * this.translationFactor;
                } else {
                    newValueRotation = this.recentState_door + diffGrabObjectAxisPullStart * this.translationFactor;
                }
                if(newValueRotation < this.minimumState) {
                    newValueRotation = this.minimumState;
                }
                if(newValueRotation > this.maximumState) {
                    newValueRotation = this.maximumState;
                }

                // setting door rotation
                switch(this.axisDoor) {
                    case Axis.X: { door.transform.localEulerAngles = new Vector3(newValueRotation, door.transform.localEulerAngles.y, door.transform.localEulerAngles.z); break; }
                    case Axis.Y: { door.transform.localEulerAngles = new Vector3(door.transform.localEulerAngles.x, newValueRotation, door.transform.localEulerAngles.z); break; }
                    case Axis.Z: { door.transform.localEulerAngles = new Vector3(door.transform.localEulerAngles.x, door.transform.localEulerAngles.y, newValueRotation); break; }
                    default:
                        break; // cannot happen
                }
            } else {
                // stopped grabbing
                this.materialTargetGameObject.GetComponent<Renderer>().material = this.originalMaterial;
                this.grabbed = false;
                this.grabObject.GetComponent<LeapHand>().setGrabbing(false);
                this.grabObject = null;
                switch(this.axisDoor) {
                    case Axis.X: { this.recentState_door = this.door.transform.localEulerAngles.x; break; }
                    case Axis.Y: { this.recentState_door = this.door.transform.localEulerAngles.y; break; }
                    case Axis.Z: { this.recentState_door = this.door.transform.localEulerAngles.z; break; }
                    default:
                        break; // cannot happen
                }
                this.recentState_grabbed = true;
                this.lastState_grabbed = true;
            }
        }
    }

    public virtual void OnTriggerStay(Collider other) {
        if(!this.grabbed) {
            // not grabbed yet

            // Leap Hand touching
            GameObject leapHand = this.isPartOfLeapHand(other.gameObject);
            if(leapHand != null) {
                // hover material
                this.materialTargetGameObject.GetComponent<MeshRenderer>().material = this.onHoverMaterial;

                // check for "new grab"
                this.lastState_grabbed = this.recentState_grabbed;
                this.recentState_grabbed = leapHand.GetComponent<GestureDetector>().isGesturingFist() && !leapHand.GetComponent<LeapHand>().isGrabbing();

                if(!this.lastState_grabbed && this.recentState_grabbed) {
                    // grabbing begins
                    leapHand.GetComponent<LeapHand>().setGrabbing(true);
                    this.grabbed = true;
                    switch(this.axisPullStart) {
                        case Axis.X: { this.grabbedAt_grabObjectAxisStart = leapHand.transform.position.x; break; }
                        case Axis.Y: { this.grabbedAt_grabObjectAxisStart = leapHand.transform.position.y; break; }
                        case Axis.Z: { this.grabbedAt_grabObjectAxisStart = leapHand.transform.position.z; break; }
                        default:
                            break; // cannot happen
                    }
                    switch(this.axisPullEnd) {
                        case Axis.X: { this.grabbedAt_grabObjectAxisEnd = leapHand.transform.position.x; break; }
                        case Axis.Y: { this.grabbedAt_grabObjectAxisEnd = leapHand.transform.position.y; break; }
                        case Axis.Z: { this.grabbedAt_grabObjectAxisEnd = leapHand.transform.position.z; break; }
                        default:
                            break; // cannot happen
                    }
                    this.grabObject = leapHand;

                    // original material
                    this.materialTargetGameObject.GetComponent<MeshRenderer>().material = this.grabMaterial;
                }
            }
        }
    }

    public void OnTriggerExit(Collider other) {
        // original material
        this.materialTargetGameObject.GetComponent<MeshRenderer>().material = this.originalMaterial;

        this.recentState_grabbed = true;
        this.lastState_grabbed = true;
    }

    protected GameObject isPartOfLeapHand(GameObject obj) {
        while(obj != null) {
            if(obj.GetComponent<LeapHand>() != null) {
                return obj;
            }
            obj = obj.transform.parent.gameObject;
        }
        return null;
    }
}
