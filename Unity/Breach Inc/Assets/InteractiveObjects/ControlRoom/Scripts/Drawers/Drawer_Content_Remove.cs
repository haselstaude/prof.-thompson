﻿using UnityEngine;
using System.Collections;
using VRTK;

public class Drawer_Content_Remove : MonoBehaviour {

    public GameObject targetParent;

    void OnTriggerStay(Collider other) {
        if(other.gameObject.GetComponent<VRTK_InteractableObject>() != null) {
            // item was removed
            other.gameObject.transform.SetParent(targetParent.transform);
        }
    }
}
