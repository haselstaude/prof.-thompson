﻿using UnityEngine;
using System.Collections;
using VRTK;

public class Drawer_Content_Add : MonoBehaviour {
    
    void OnTriggerStay(Collider other) {
        if (other.gameObject.GetComponent<VRTK_InteractableObject>() != null) {
            // item was inserted
            other.gameObject.transform.SetParent(this.gameObject.transform);
        }
    }
}
