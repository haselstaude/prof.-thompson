﻿using UnityEngine;
using System.Collections;
using VRTK;

public class Drawer_Movement_ElectricalLock : MonoBehaviour {

    public enum Axis {
        X,
        Y,
        Z
    }

    [Header("Materials", order = 0)]
    public Material originalMaterial;
    public Material onHoverMaterial;
    public Material grabMaterial;
    public GameObject materialTargetGameObject;
    public GameObject movementTargetGameObject;

    [Header("Movement", order = 1)]
    public Axis axis;
    public bool invertedAxis;
    public float drawerTranslationFactor = 1f;
    public float minimumState = 0f;
    public float recentState_drawer = 0f;
    public float maximumState = 1f;

    [Header("Collider", order = 2)]
    public bool moveCollider = false;
    public float recentState_collider = 0f;
    public float colliderTranslationFactor = 1f;

    [Header("Lock", order = 3)]
    public bool locked = true;
    public GameObject statusLamp;
    public Material lockedMaterial;
    public Material unlockedMaterial;

    private bool recentState_grabbed = false;
    private bool lastState_grabbed = false;

    private bool grabbed = false;
    private float grabbedAt_grabObject = 0f;
    private GameObject grabObject;

    // Use this for initialization
    void Start() {
        if(this.locked) {
            Lock();
        } else {
            Unlock();
        }
    }

    // Update is called once per frame
    void Update() {
        if(this.grabbed) {
            if(!this.grabObject.GetComponent<GestureDetector>().isGesturingFlatHand()) {
                // still grabbing
                float diffGrabObject = 0f;
                float newValueDrawer = 0f;

                // calculating difference of grab
                switch(this.axis) {
                    case Axis.X: { diffGrabObject = this.grabObject.transform.position.x - this.grabbedAt_grabObject; break; }
                    case Axis.Y: { diffGrabObject = this.grabObject.transform.position.y - this.grabbedAt_grabObject; break; }
                    case Axis.Z: { diffGrabObject = this.grabObject.transform.position.z - this.grabbedAt_grabObject; break; }
                    default:
                        break; // cannot happen
                }

                // calculating new position of drawer
                if(this.invertedAxis) {
                    newValueDrawer = this.recentState_drawer - diffGrabObject * this.drawerTranslationFactor;
                } else {
                    newValueDrawer = this.recentState_drawer + diffGrabObject * this.drawerTranslationFactor;
                }
                if(newValueDrawer < this.minimumState) {
                    newValueDrawer = this.minimumState;
                }
                if(newValueDrawer > this.maximumState) {
                    newValueDrawer = this.maximumState;
                }

                // calculating and setting new position of collider
                if(this.moveCollider) {
                    float newValueCollider = 0f;
                    if(this.invertedAxis) {
                        newValueCollider = this.recentState_collider - diffGrabObject * this.colliderTranslationFactor;
                    } else {
                        newValueCollider = this.recentState_collider + diffGrabObject * this.colliderTranslationFactor;
                    }

                    // set
                    Vector3 colliderCenter = this.gameObject.GetComponent<BoxCollider>().center;
                    switch(this.axis) {
                        case Axis.X: { colliderCenter.x = newValueCollider; break; }
                        case Axis.Y: { colliderCenter.y = newValueCollider; break; }
                        case Axis.Z: { colliderCenter.z = newValueCollider; break; }
                        default:
                            break; // cannot happen
                    }
                    this.gameObject.GetComponent<BoxCollider>().center = colliderCenter;
                }

                // setting drawer position
                switch(this.axis) {
                    case Axis.X: { movementTargetGameObject.transform.localPosition = new Vector3(newValueDrawer, movementTargetGameObject.transform.localPosition.y, movementTargetGameObject.transform.localPosition.z); break; }
                    case Axis.Y: { movementTargetGameObject.transform.localPosition = new Vector3(movementTargetGameObject.transform.localPosition.x, newValueDrawer, movementTargetGameObject.transform.localPosition.z); break; }
                    case Axis.Z: { movementTargetGameObject.transform.localPosition = new Vector3(movementTargetGameObject.transform.localPosition.x, movementTargetGameObject.transform.localPosition.y, newValueDrawer); break; }
                    default:
                        break; // cannot happen
                }
            } else {
                // stopped grabbing
                this.materialTargetGameObject.GetComponent<Renderer>().material = this.originalMaterial;
                this.grabbed = false;
                this.grabObject.GetComponent<LeapHand>().setGrabbing(false);
                this.grabObject = null;
                switch(this.axis) {
                    case Axis.X: { this.recentState_drawer = this.movementTargetGameObject.transform.localPosition.x; break; }
                    case Axis.Y: { this.recentState_drawer = this.movementTargetGameObject.transform.localPosition.y; break; }
                    case Axis.Z: { this.recentState_drawer = this.movementTargetGameObject.transform.localPosition.z; break; }
                    default:
                        break; // cannot happen
                }
                if(this.moveCollider) {
                    switch(this.axis) {
                        case Axis.X: { this.recentState_collider = this.gameObject.GetComponent<BoxCollider>().center.x; break; }
                        case Axis.Y: { this.recentState_collider = this.gameObject.GetComponent<BoxCollider>().center.y; break; }
                        case Axis.Z: { this.recentState_collider = this.gameObject.GetComponent<BoxCollider>().center.z; break; }
                        default:
                            break; // cannot happen
                    }
                }
                this.recentState_grabbed = true;
                this.lastState_grabbed = true;
            }
        }
    }

    public void OnTriggerStay(Collider other) {
        if(!this.grabbed && !this.locked) {
            // not grabbed yet

            // Leap Hand touching
            GameObject leapHand = this.isPartOfLeapHand(other.gameObject);
            if(leapHand != null) {
                // hover material
                this.materialTargetGameObject.GetComponent<MeshRenderer>().material = this.onHoverMaterial;

                // check for "new grab"
                this.lastState_grabbed = this.recentState_grabbed;
                this.recentState_grabbed = leapHand.GetComponent<GestureDetector>().isGesturingFist() && !leapHand.GetComponent<LeapHand>().isGrabbing();

                if(!this.lastState_grabbed && this.recentState_grabbed) {
                    // grabbing begins
                    leapHand.GetComponent<LeapHand>().setGrabbing(true);
                    this.grabbed = true;
                    switch(this.axis) {
                        case Axis.X: { this.grabbedAt_grabObject = leapHand.transform.position.x; break; }
                        case Axis.Y: { this.grabbedAt_grabObject = leapHand.transform.position.y; break; }
                        case Axis.Z: { this.grabbedAt_grabObject = leapHand.transform.position.z; break; }
                        default:
                            break; // cannot happen
                    }
                    this.grabObject = leapHand;

                    // original material
                    this.materialTargetGameObject.GetComponent<MeshRenderer>().material = this.grabMaterial;
                }
            }
        }
    }

    public void OnTriggerExit(Collider other) {
        // original material
        this.materialTargetGameObject.GetComponent<MeshRenderer>().material = this.originalMaterial;

        this.recentState_grabbed = true;
        this.lastState_grabbed = true;
    }

    public void Unlock() {
        this.locked = false;
        this.statusLamp.GetComponent<MeshRenderer>().material = this.unlockedMaterial;
    }

    public void Lock() {
        this.locked = true;
        this.statusLamp.GetComponent<MeshRenderer>().material = this.lockedMaterial;
    }

    protected GameObject isPartOfLeapHand(GameObject obj) {
        while(obj != null) {
            if(obj.GetComponent<LeapHand>() != null) {
                return obj;
            }
            obj = obj.transform.parent.gameObject;
        }
        return null;
    }
}
