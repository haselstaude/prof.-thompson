﻿using UnityEngine;
using System.Collections;
using VRTK;

public abstract class AbstractDigitalLever : MonoBehaviour {

    public enum Axis {
        X,
        Y,
        Z
    }

    [Header("Materials", order = 0)]
    public Material originalMaterial;
    public Material onHoverMaterial;
    public Material grabMaterial;

    [Header("Movement", order = 1)]
    public Axis axisWorld;
    public Axis axisLever;
    public bool invertedAxis;
    public float translationFactor = 1f;
    public float minimumState = 0f;
    public float recentState = 0f;
    public float maximumState = 1f;
    public int percentPulledNeededForAction = 80;
    public bool on = false;

    private bool recentState_grabbed = false;
    private bool lastState_grabbed = false;

    private bool grabbed = false;
    private float grabbedAt_grabObject = 0f;
    private GameObject grabObject;
	
	// Update is called once per frame
	void Update () {
        if(this.grabbed) {
            if(!this.grabObject.GetComponent<GestureDetector>().isGesturingFlatHand()) {
                // still grabbing
                float diffGrabObject = 0f;
                float newValueDrawer = 0f;

                // calculating difference of grab
                switch(this.axisWorld) {
                    case Axis.X: { diffGrabObject = this.grabObject.transform.position.x - this.grabbedAt_grabObject; break; }
                    case Axis.Y: { diffGrabObject = this.grabObject.transform.position.y - this.grabbedAt_grabObject; break; }
                    case Axis.Z: { diffGrabObject = this.grabObject.transform.position.z - this.grabbedAt_grabObject; break; }
                    default:
                        break; // cannot happen
                }

                // calculating new position of drawer
                if(this.invertedAxis) {
                    newValueDrawer = this.recentState - diffGrabObject * this.translationFactor;
                } else {
                    newValueDrawer = this.recentState + diffGrabObject * this.translationFactor;
                }
                if(newValueDrawer < this.minimumState) {
                    newValueDrawer = this.minimumState;
                }
                if(newValueDrawer > this.maximumState) {
                    newValueDrawer = this.maximumState;
                }

                // check if action should be performed
                float neededMinPosForAction = (this.maximumState - this.minimumState) * this.percentPulledNeededForAction / 100 + this.minimumState;
                if (newValueDrawer >= neededMinPosForAction && !this.on) {
                    doOnAction();
                    this.on = true;
                } else if (newValueDrawer < neededMinPosForAction && this.on) {
                    doOffAction();
                    this.on = false;
                }

                // setting lever position
                switch(this.axisLever) {
                    case Axis.X: { this.gameObject.transform.localPosition = new Vector3(newValueDrawer, this.gameObject.transform.localPosition.y, this.gameObject.transform.localPosition.z); break; }
                    case Axis.Y: { this.gameObject.transform.localPosition = new Vector3(this.gameObject.transform.localPosition.x, newValueDrawer, this.gameObject.transform.localPosition.z); break; }
                    case Axis.Z: { this.gameObject.transform.localPosition = new Vector3(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y, newValueDrawer); break; }
                    default: break; // cannot happen
                }
            } else {
                // stopped grabbing
                this.gameObject.GetComponent<Renderer>().material = this.originalMaterial;
                this.grabbed = false;
                this.grabObject.GetComponent<LeapHand>().setGrabbing(false);
                this.grabObject = null;
                switch(this.axisLever) {
                    case Axis.X: { this.recentState = this.gameObject.transform.localPosition.x; break; }
                    case Axis.Y: { this.recentState = this.gameObject.transform.localPosition.y; break; }
                    case Axis.Z: { this.recentState = this.gameObject.transform.localPosition.z; break; }
                    default:
                        break; // cannot happen
                }
                this.recentState_grabbed = true;
                this.lastState_grabbed = true;
            }
        }
    }

    public void OnTriggerStay(Collider other) {
        if(!this.grabbed) {
            // not grabbed yet

            // LeapHand touching
            GameObject leapHand = this.isPartOfLeapHand(other.gameObject);
            if(leapHand != null) {
                // hover material
                this.gameObject.GetComponent<MeshRenderer>().material = this.onHoverMaterial;

                // check for "new grab"
                this.lastState_grabbed = this.recentState_grabbed;
                this.recentState_grabbed = leapHand.GetComponent<GestureDetector>().isGesturingFist() && !leapHand.GetComponent<LeapHand>().isGrabbing();

                if(!this.lastState_grabbed && this.recentState_grabbed) {
                    // grabbing begins
                    leapHand.GetComponent<LeapHand>().setGrabbing(true);
                    this.grabbed = true;
                    switch(this.axisWorld) {
                        case Axis.X: { this.grabbedAt_grabObject = leapHand.transform.position.x; break; }
                        case Axis.Y: { this.grabbedAt_grabObject = leapHand.transform.position.y; break; }
                        case Axis.Z: { this.grabbedAt_grabObject = leapHand.transform.position.z; break; }
                        default:
                            break; // cannot happen
                    }
                    this.grabObject = leapHand;

                    // grab material
                    this.gameObject.GetComponent<MeshRenderer>().material = this.grabMaterial;
                }
            }
        }
    }

    public void OnTriggerExit(Collider other) {
        // original material
        this.gameObject.GetComponent<MeshRenderer>().material = this.originalMaterial;

        this.recentState_grabbed = true;
        this.lastState_grabbed = true;
    }

    public abstract void doOnAction();

    public abstract void doOffAction();

    protected GameObject isPartOfLeapHand(GameObject obj) {
        while(obj != null) {
            if(obj.GetComponent<LeapHand>() != null) {
                return obj;
            }
            obj = obj.transform.parent.gameObject;
        }
        return null;
    }
}
