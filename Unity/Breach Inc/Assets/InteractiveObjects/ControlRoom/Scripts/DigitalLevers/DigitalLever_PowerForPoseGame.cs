﻿using UnityEngine;
using System.Collections;
using System;

public class DigitalLever_PowerForPoseGame : AbstractDigitalLever {

    [Header("Actions", order = 2)]
    public GameObject poseGame;

    [Header("For Easteregg", order = 3)]
    public GameObject lockedDrawer;
    public GameObject bottomScreenManager;
    public GameObject topScreenManager;
    public GameObject cactus;
    public GameObject spawnPosition;
    public float spawningTimeDistance = 0.5f;
    public int spawningAmount = 20;

    private int spawnedYet = 0;

    public override void doOnAction() {
        this.poseGame.GetComponent<PoseGame>().powered = true;

        // Easteregg
        if (this.lockedDrawer.GetComponent<Drawer_Movement_ElectricalLock>().locked) {
            // Lever was pulled within closed drawer - smart...

            // show easteregg-screens
            this.topScreenManager.GetComponent<ScreenManager>().showScreen(ScreenManager.Screens.EASTEREGG_POSE_POWERSWITCH_WITHOUT_UNLOCKING_DRAWER, true);
            this.bottomScreenManager.GetComponent<ScreenManager>().showScreen(ScreenManager.Screens.EASTEREGG_POSE_POWERSWITCH_WITHOUT_UNLOCKING_DRAWER, true);

            // start spawning cactusses
            InvokeRepeating("eastereggTriggered", 3.0f, this.spawningTimeDistance);
        }
    }

    public override void doOffAction() {
        // not needed
    }

    private void eastereggTriggered() {
        Instantiate(this.cactus, this.spawnPosition.transform);

        this.spawnedYet++;
        if (this.spawnedYet >= this.spawningAmount) {
            CancelInvoke();
        }
    }
}
