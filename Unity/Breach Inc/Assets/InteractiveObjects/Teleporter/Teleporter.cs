﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Teleporter : MonoBehaviour {

    public int id = -1;

    private GameObject insertedItem = null;

    private bool teleportFailed = false;

    private GameObject textGUI;

    // Use this for initialization
    void Start () {
        // get text-element of GUI
        this.textGUI = GameObject.Find("GUI").transform.FindChild("Text").gameObject;
    }

    void OnTriggerEnter(Collider other) {
        if(other.gameObject.GetComponent<Inventory>() != null) {
            // Player stepped to Teleporter
            this.teleportFailed = false;
        }
    }

    void OnTriggerStay(Collider other) {
        if(other.gameObject.GetComponent<Inventory>() != null) {
            // Player near Teleporter

            // set this one as last used
            GameObject.Find("Manager").GetComponent<TeleporterManager>().lastUsedTeleporter = this.id;

            if(!this.teleportFailed) {
                // actions granted
                // GUI
                if(this.insertedItem == null && other.gameObject.GetComponent<Inventory>().getSelectedItem() != null) {
                    // Teleporter is empty and player has an item selected
                    this.textGUI.SetActive(true);
                    this.textGUI.GetComponent<Text>().text = "Press X to put in";
                } else if(this.insertedItem != null) {
                    // Teleporter has item in it
                    this.textGUI.SetActive(true);
                    this.textGUI.GetComponent<Text>().text = "Press X to activate\nPress B to take item out";
                }

                if(Input.GetButtonUp("Interact")) {
                    // Player pressed Interact-Button
                    if(this.insertedItem == null) {
                        // no item inserted yet

                        GameObject item = other.gameObject.GetComponent<Inventory>().getSelectedItem();
                        if(item == null) {
                            // No item in current slot
                            return;
                        }
                        if(item.tag == "Teleportable") {
                            // Remove item from Inventory
                            other.gameObject.GetComponent<Inventory>().getSelectedItem(true);

                            // Put item  in Teleporter
                            insertItem(item);

                            // GUI
                            this.textGUI.SetActive(true);
                            this.textGUI.GetComponent<Text>().text = "Press X to activate\nPress B to take item out";
                        } else {
                            // item can't be teleported
                            this.textGUI.SetActive(true);
                            this.textGUI.GetComponent<Text>().text = "This item can't be teleported";
                            this.teleportFailed = true;
                        }
                    } else {
                        // already a item inserted

                        if(this.insertedItem.tag == "Teleportable") {
                            // item teleportable
                            // this will send it to the other teleporter

                            sendToVRTeleporter();

                            // GUI
                            this.textGUI.SetActive(false);
                        }
                    }
                }

                if(Input.GetButtonUp("InteractBack")) {
                    // Player pressed InteractBack-Button

                    if(this.insertedItem != null) {
                        // item was inserted before

                        takeOut(other.gameObject);
                    }
                }
            }
        }
    }


    void OnTriggerExit(Collider other) {
        if(other.gameObject.GetComponent<Inventory>() != null) {
            // Player steps away from Teleporter
            this.textGUI.SetActive(false);
        }
    }

    public void insertItem(GameObject item) {
        // make item visible
        item.SetActive(true);

        // insert item
        this.insertedItem = item;

        // show item visually
        GameObject receiver = this.gameObject.transform.FindChild("Receiver").gameObject;
        item.transform.position = receiver.transform.position;
    }

    private void takeOut(GameObject player) {
        // add it back to inventory if space
        if(player.GetComponent<Inventory>().addItem(this.insertedItem)) {
            // could be added

            // remove item
            this.insertedItem.SetActive(false);
            this.insertedItem = null;

            // GUI
            if(player.gameObject.GetComponent<Inventory>().getSelectedItem() != null) {
                // Teleporter is empty and player has an item selected
                this.textGUI.SetActive(true);
                this.textGUI.GetComponent<Text>().text = "Press X to put in";
            } else {
                this.textGUI.SetActive(false);
            }
        } else {
            // Inventory is full
            this.textGUI.SetActive(true);
            this.textGUI.GetComponent<Text>().text = "Inventory full";
        }
    }

    private void sendToVRTeleporter() {
        GameObject targetTP = GameObject.Find("Manager").GetComponent<TeleporterManager>().getVRTeleporter();
        targetTP.GetComponent<Teleporter>().insertItem(this.insertedItem);

        // remove from this teleporter
        this.insertedItem = null;
    }

}
