﻿using UnityEngine;
using System.Collections;

public class TeleporterManager : MonoBehaviour {

    public int AMOUNT_TELEPORTER_CONTROLLERPLAYER = -1;

    public int lastUsedTeleporter = -1;

    private GameObject[] teleporters;

	// Use this for initialization
	void Start () {
        this.teleporters = new GameObject[this.AMOUNT_TELEPORTER_CONTROLLERPLAYER + 1];
        loadAllTeleporters();
	}


    public GameObject getVRTeleporter() {
        return this.teleporters[0];
    }

    public GameObject getLastUsedTeleporter() {
        return this.teleporters[this.lastUsedTeleporter];
    }

    private void loadAllTeleporters() {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Teleporter");
        foreach(GameObject obj in objects) {
            if(obj.GetComponent<Teleporter>() != null) {
                // controller player-teleporter
                this.teleporters[obj.GetComponent<Teleporter>().id] = obj;
            } else if(obj.GetComponent<VRTeleporter>() != null) {
                // vr player-teleporter
                this.teleporters[0] = obj;
            }
        }
    }
}
