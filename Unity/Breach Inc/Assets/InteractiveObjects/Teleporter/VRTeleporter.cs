﻿using UnityEngine;
using System.Collections;
using VRTK;
using VRTK.GrabAttachMechanics;

public class VRTeleporter : Teleporter {

    [Header("Animation - Timing", order = 1)]
    public float animationArmBaseLength = 0.5f;
    public float animationArmMidStart = 0.5f;
    public float animationArmMidLength = 0.5f;
    public float animationArmTopStart = 1.0f;
    public float animationArmTopLength = 0.5f;

    [Header("Animation - Values", order = 2)]
    private float armBaseStart;
    public float armBaseTarget = 8.0f;
    private float armMidStart;
    public float armMidTarget = 40.0f;
    private float armTopStart;
    public float armTopTarget = 90.0f;

    [Header("Animation - Undo - Timing", order = 3)]
    public float animationUndoLength = 1.0f;

    [Header("Animation - Object - Left Arm", order = 4)]
    public GameObject LeftArmBase;
    public GameObject LeftArmMid;
    public GameObject LeftArmTop;

    [Header("Animation - Object - Middle Arm", order = 5)]
    public GameObject MiddleArmBase;
    public GameObject MiddleArmMid;
    public GameObject MiddleArmTop;

    [Header("Animation - Object - Right Arm", order = 6)]
    public GameObject RightArmBase;
    public GameObject RightArmMid;
    public GameObject RightArmTop;

    [Header("Animation - Particles", order = 7)]
    public GameObject particleSystemBase;
    public int particleAmount = 100;

    private bool animate = false;
    private float animationTime = 0f;

    private bool animateUndo = false;
    private float animationUndoTime = 0f;

    private GameObject invokePass = null;

    void Start() {
        // get start-angles for animation
        this.armBaseStart = this.LeftArmBase.transform.localEulerAngles.z;
        this.armMidStart = this.LeftArmMid.transform.localEulerAngles.z;
        this.armTopStart = this.LeftArmTop.transform.localEulerAngles.z;
    }

    void Update() {
        if (this.animate) {
            // animation
            this.animationTime += Time.deltaTime;

            if (this.animationTime <= this.animationArmBaseLength) {
                // animate base arm
                float distance = this.armBaseTarget - this.armBaseStart;
                float timeFactor = this.animationTime / this.animationArmBaseLength;
                float recentAngle = this.armBaseStart + distance * timeFactor;

                // set base arm angles
                this.LeftArmBase.transform.localEulerAngles = new Vector3(this.LeftArmBase.transform.localEulerAngles.x, this.LeftArmBase.transform.localEulerAngles.y, recentAngle);
                this.MiddleArmBase.transform.localEulerAngles = new Vector3(this.MiddleArmBase.transform.localEulerAngles.x, this.MiddleArmBase.transform.localEulerAngles.y, recentAngle);
                this.RightArmBase.transform.localEulerAngles = new Vector3(this.RightArmBase.transform.localEulerAngles.x, this.RightArmBase.transform.localEulerAngles.y, recentAngle);
            }
            if (this.animationTime >= this.animationArmMidStart && this.animationTime <= this.animationArmMidStart + this.animationArmMidLength) {
                // animate mid arm
                float distance = this.armMidTarget - this.armMidStart;
                float timeFactor = (this.animationTime - this.animationArmMidStart) / this.animationArmMidLength;
                float recentAngle = this.armMidStart + distance * timeFactor;

                // set mid arm angles
                this.LeftArmMid.transform.localEulerAngles = new Vector3(this.LeftArmMid.transform.localEulerAngles.x, this.LeftArmMid.transform.localEulerAngles.y, recentAngle);
                this.MiddleArmMid.transform.localEulerAngles = new Vector3(this.MiddleArmMid.transform.localEulerAngles.x, this.MiddleArmMid.transform.localEulerAngles.y, recentAngle);
                this.RightArmMid.transform.localEulerAngles = new Vector3(this.RightArmMid.transform.localEulerAngles.x, this.RightArmMid.transform.localEulerAngles.y, recentAngle);
            }
            if(this.animationTime >= this.animationArmTopStart && this.animationTime <= this.animationArmTopStart + this.animationArmTopLength) {
                // animate top arm
                float distance = this.armTopTarget - this.armTopStart;
                float timeFactor = (this.animationTime - this.animationArmTopStart) / this.animationArmTopLength;
                float recentAngle = this.armTopStart + distance * timeFactor;

                // set top arm angles
                this.LeftArmTop.transform.localEulerAngles = new Vector3(this.LeftArmTop.transform.localEulerAngles.x, this.LeftArmTop.transform.localEulerAngles.y, recentAngle);
                this.MiddleArmTop.transform.localEulerAngles = new Vector3(this.MiddleArmTop.transform.localEulerAngles.x, this.MiddleArmTop.transform.localEulerAngles.y, recentAngle);
                this.RightArmTop.transform.localEulerAngles = new Vector3(this.RightArmTop.transform.localEulerAngles.x, this.RightArmTop.transform.localEulerAngles.y, recentAngle);
            }
            if (this.animationTime > this.animationArmTopStart + this.animationArmTopLength) {
                // particles
                startBaseParticles();
                Invoke("endBaseParticles", 0.5f);

                // end of animation
                this.animate = false;
                this.animationTime = 0f;
            }
        } else if (this.animateUndo) {
            // undo animation
            this.animationUndoTime += Time.deltaTime;
            float timeFactor = this.animationUndoTime / this.animationUndoLength;

            // undo-animate base arms
            float distance = this.armBaseTarget - this.armBaseStart;
            float recentAngle = this.armBaseTarget - distance * timeFactor;
            // set base arm angles
            this.LeftArmBase.transform.localEulerAngles = new Vector3(this.LeftArmBase.transform.localEulerAngles.x, this.LeftArmBase.transform.localEulerAngles.y, recentAngle);
            this.MiddleArmBase.transform.localEulerAngles = new Vector3(this.MiddleArmBase.transform.localEulerAngles.x, this.MiddleArmBase.transform.localEulerAngles.y, recentAngle);
            this.RightArmBase.transform.localEulerAngles = new Vector3(this.RightArmBase.transform.localEulerAngles.x, this.RightArmBase.transform.localEulerAngles.y, recentAngle);

            // undo-animate mid arms
            distance = this.armMidTarget - this.armMidStart;
            recentAngle = this.armMidTarget - distance * timeFactor;
            // set mid arm angles
            this.LeftArmMid.transform.localEulerAngles = new Vector3(this.LeftArmMid.transform.localEulerAngles.x, this.LeftArmMid.transform.localEulerAngles.y, recentAngle);
            this.MiddleArmMid.transform.localEulerAngles = new Vector3(this.MiddleArmMid.transform.localEulerAngles.x, this.MiddleArmMid.transform.localEulerAngles.y, recentAngle);
            this.RightArmMid.transform.localEulerAngles = new Vector3(this.RightArmMid.transform.localEulerAngles.x, this.RightArmMid.transform.localEulerAngles.y, recentAngle);

            // undo-animate top arms
            distance = this.armTopTarget - this.armTopStart;
            recentAngle = this.armTopTarget - distance * timeFactor;
            // set top arm angles
            this.LeftArmTop.transform.localEulerAngles = new Vector3(this.LeftArmTop.transform.localEulerAngles.x, this.LeftArmTop.transform.localEulerAngles.y, recentAngle);
            this.MiddleArmTop.transform.localEulerAngles = new Vector3(this.MiddleArmTop.transform.localEulerAngles.x, this.MiddleArmTop.transform.localEulerAngles.y, recentAngle);
            this.RightArmTop.transform.localEulerAngles = new Vector3(this.RightArmTop.transform.localEulerAngles.x, this.RightArmTop.transform.localEulerAngles.y, recentAngle);

            if (this.animationUndoTime >= this.animationUndoLength) {
                // end of animation
                this.animateUndo = false;
                this.animationUndoTime = 0f;
            }
        }
    }

    void OnTriggerEnter(Collider other) {
        GameObject item = other.gameObject;

        // checking if item is teleportable
        if (item.tag == "Teleportable") {
            // is teleportable

            // drop it
            other.gameObject.GetComponent<Custom_GrabbableObject>().ForceStopInteracting();
            other.gameObject.GetComponent<Custom_GrabbableObject>().ToggleHighlight(false);

            // start animation
            this.animate = true;

            // invoke
            this.invokePass = item;
            Invoke("teleport", 2f);
            Invoke("startUndoAnimation", 2f);
        } else {
            // is not teleportable
            // do nothing
        }
    }

    private void teleport() {
        GameObject item = this.invokePass;
        GameObject targetTP = GameObject.Find("Manager").GetComponent<TeleporterManager>().getLastUsedTeleporter();
        targetTP.GetComponent<Teleporter>().insertItem(item);
        item.GetComponent<Custom_GrabbableObject>().ToggleHighlight(false);
        this.invokePass = null;
    }

    private void startUndoAnimation() {
        this.animateUndo = true;
    }

    private void startBaseParticles() {
        ParticleSystem.EmissionModule em = this.particleSystemBase.GetComponent<ParticleSystem>().emission;
        em.enabled = true;
    }

    private void endBaseParticles() {
        ParticleSystem.EmissionModule em = this.particleSystemBase.GetComponent<ParticleSystem>().emission;
        em.enabled = false;
    }
}
