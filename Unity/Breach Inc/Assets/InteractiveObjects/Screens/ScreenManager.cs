﻿using UnityEngine;
using System.Collections;

public class ScreenManager : MonoBehaviour {

    public class Screens {
        public const int EMPTY = -1;
        public const int WELCOME = 0;
        public const int MINEROOM_ENTERING = 1;
        public const int MINEROOM_LOADING = 2;
        public const int MINEROOM = 3;
        public const int EASTEREGG_POSE_POWERSWITCH_WITHOUT_UNLOCKING_DRAWER = 4;
        public const int YOURFRIENDDIED = 99;
    }

    public GameObject background;
    public GameObject welcomeScreen;
    public GameObject mineRoomEnteringScreen;
    public GameObject mineRoomLoadingScreen;
    public GameObject mineRoomScreen;
    public GameObject eastereggPosePowerswitchWithoutUnlockingDrawerScreen;
    public GameObject yourFriendDiedScreen;

	// Use this for initialization
	void Start () {
        // show welcome-screen
        showScreenAsObj(background);
        showScreenAsObj(welcomeScreen);

        // set all hidden
        hideScreenAsObj(mineRoomEnteringScreen);
        hideScreenAsObj(mineRoomLoadingScreen);
        hideScreenAsObj(mineRoomScreen);
        hideScreenAsObj(eastereggPosePowerswitchWithoutUnlockingDrawerScreen);
        hideScreenAsObj(yourFriendDiedScreen);

        // set start screen showing
        showScreenAsObj(welcomeScreen);
	}

    public void hideScreen(int screen) {
        switch(screen) {
            case 1: hideScreenAsObj(mineRoomEnteringScreen); break;
            case 2: hideScreenAsObj(mineRoomLoadingScreen); break;
            case 3: hideScreenAsObj(mineRoomScreen); break;
            case 4: hideScreenAsObj(eastereggPosePowerswitchWithoutUnlockingDrawerScreen); break;
            case 99: hideScreenAsObj(yourFriendDiedScreen); break;
            default: hideScreenAsObj(welcomeScreen); break;
        }
    }

    public void showScreen(int screen, bool onlyShown = true) {
        if(onlyShown) {
            onlyShowBackground();
        }
        switch(screen) {
            case -1: break; // show only background
            case 1: showScreenAsObj(mineRoomEnteringScreen); break;
            case 2: showScreenAsObj(mineRoomLoadingScreen); break;
            case 3: showScreenAsObj(mineRoomScreen); break;
            case 4: showScreenAsObj(eastereggPosePowerswitchWithoutUnlockingDrawerScreen); break;
            case 99: showScreenAsObj(yourFriendDiedScreen); break;
            default: showScreenAsObj(welcomeScreen); break;
        }
    }

    private void onlyShowBackground() {
        showScreenAsObj(background);
        hideScreenAsObj(welcomeScreen);
        hideScreenAsObj(mineRoomEnteringScreen);
        hideScreenAsObj(mineRoomLoadingScreen);
        hideScreenAsObj(mineRoomScreen);
        hideScreenAsObj(eastereggPosePowerswitchWithoutUnlockingDrawerScreen);
        hideScreenAsObj(yourFriendDiedScreen);
    }

    private void hideScreenAsObj(GameObject screen) {
        if(screen != null) {
            screen.SetActive(false);
        }
    }

    private void showScreenAsObj(GameObject screen) {
        if(screen != null) {
            screen.SetActive(true);
        }
    }
}
