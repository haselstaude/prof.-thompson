﻿using UnityEngine;
using System.Collections;

public class L1S2Checkpoint : SimpleCheckpoint {

    public GameObject MineRoomButton;
    public GameObject MineRoom;
    public GameObject screenManagerTop;
    public GameObject screenManagerBottom;

    public override void recoverData() {
        base.recoverData();

        // recover MineRoom
        this.MineRoomButton.GetComponent<MineRoomButton>().pressed = false;
        this.MineRoom.GetComponent<MineRoomManager>().reset();

        // set screens in control room to enter
        this.screenManagerTop.GetComponent<ScreenManager>().showScreen(ScreenManager.Screens.MINEROOM_ENTERING);
        this.screenManagerBottom.GetComponent<ScreenManager>().showScreen(ScreenManager.Screens.MINEROOM_ENTERING);
    }

}
