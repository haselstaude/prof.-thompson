﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SimpleCheckpoint : MonoBehaviour {

    public GameObject player;
    public GameObject respawnPoint;
    private GameObject textGUI;
    private bool saved = false;

    void Start() {
        // get text-element of GUI
        this.textGUI = GameObject.Find("GUI").transform.FindChild("Text").gameObject;
    }

	void OnTriggerStay(Collider other) {
        if(other.gameObject.GetComponent<Player>() != null) {
            // player near socket

            if(!this.saved) {
                if(other.gameObject.GetComponent<Inventory>().getSelectedItem() != null) {
                    // has item
                    if(other.gameObject.GetComponent<Inventory>().getSelectedItem().GetComponent<SaveData>() != null) {
                        // has usb-stick

                        // GUI
                        this.textGUI.SetActive(true);
                        this.textGUI.GetComponent<Text>().text = "Press X to save your progress";

                        if(Input.GetButtonUp("Interact")) {
                            // saving
                            other.gameObject.GetComponent<Inventory>().getSelectedItem().GetComponent<SaveData>().checkpoint = this.gameObject;
                            this.saved = true;
                        }
                    } else {
                        // GUI
                        this.textGUI.SetActive(true);
                        this.textGUI.GetComponent<Text>().text = "You need your USB-Stick to save your progress";
                    }
                } else {
                    // GUI
                    this.textGUI.SetActive(true);
                    this.textGUI.GetComponent<Text>().text = "You need an USB-Stick to save your progress";
                }
            } else {
                // already saved here

                // GUI
                this.textGUI.SetActive(true);
                this.textGUI.GetComponent<Text>().text = "Progress saved...";
            }
        }
    }

    void OnTriggerExit(Collider other) {
        if(other.gameObject.GetComponent<Inventory>() != null) {
            // Player steps away from Button
            this.textGUI.SetActive(false);
        }
    }

    public virtual void recoverData() {
        this.player.transform.position = this.respawnPoint.transform.position;
    }

}
