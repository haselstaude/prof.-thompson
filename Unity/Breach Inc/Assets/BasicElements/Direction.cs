﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.BasicElements {
    class Direction {
        public const int NORTH = 0;
        public const int EAST = 1;
        public const int SOUTH = 2;
        public const int WEST = 3;
    }
}
