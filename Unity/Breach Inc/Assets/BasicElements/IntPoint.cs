﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.BasicElements {
    class IntPoint {
        public int x = 0;
        public int y = 0;

        public IntPoint() { }

        public IntPoint(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
