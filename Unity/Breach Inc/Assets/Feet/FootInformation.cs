﻿using UnityEngine;
using System.Collections;

public class FootInformation : MonoBehaviour {

    public enum FootSide {
        LEFT,RIGHT
    }

    [Header("General", order = 0)]
    public FootSide foot;
    public GameObject calibrationResult;
    public float heightToleranceInCm = 3f;

    [Header("DebugMaterials", order = 1)]
    public Material materialOnGround;
    public Material materialLiftUp;
    public Material materialFullyLiftUp;

    void Update() {
        try {
            if(isFootOnGround()) {
                this.setMaterialFoot(materialOnGround);
            } else {
                this.setMaterialFoot(materialLiftUp);
                if(isKneeFullyPulledUp()) {
                    this.setMaterialFoot(materialFullyLiftUp);
                }
            }
        } catch(System.Exception ex) {
            //ignore
        }
    }

    public bool isFootOnGround() {
        if(this.calibrationResult.GetComponent<CalibrationResult>().height.isCalibrated) {
            if(this.foot == FootSide.LEFT) {
                //check if left foot on ground
                if(this.gameObject.transform.localPosition.y <= this.calibrationResult.GetComponent<CalibrationResult>().height.footLeft + this.heightToleranceInCm / 100f) {
                    return true;
                } else {
                    return false;
                }
            } else {
                //check if right foot on ground
                if(this.gameObject.transform.localPosition.y <= this.calibrationResult.GetComponent<CalibrationResult>().height.footRight + this.heightToleranceInCm / 100f) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            throw new System.Exception("Height not calibrated yet, but isFootOnGround was called.");
        }
    }

    public bool isFootRisen() {
        return !this.isFootOnGround();
    }

    public bool isKneeFullyPulledUp() {
        if(this.calibrationResult.GetComponent<CalibrationResult>().kneeHeight.isCalibrated) {
            if(this.foot == FootSide.LEFT) {
                //check if left foot fully pulled up
                if(this.gameObject.transform.localPosition.y >= this.calibrationResult.GetComponent<CalibrationResult>().kneeHeight.kneeLeft - this.heightToleranceInCm / 100f) {
                    return true;
                } else {
                    return false;
                }
            } else {
                //check if right foot fully pulled up
                if(this.gameObject.transform.localPosition.y >= this.calibrationResult.GetComponent<CalibrationResult>().kneeHeight.kneeRight - this.heightToleranceInCm / 100f) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            throw new System.Exception("Knee-Height not calibrated yet, but isKneeFullyPulledUp was called.");
        }
    }

    /// <summary>
    /// Returns how much the knee is pulled up
    /// 0f = on ground
    /// 1f = fully pulled up
    /// </summary>
    /// <returns>Factor how far the knee is pulled up</returns>
    public float getKneePulledUpFactor() {
        if(this.calibrationResult.GetComponent<CalibrationResult>().kneeHeight.isCalibrated) {
            float height = this.gameObject.transform.localPosition.y;
            if(this.foot == FootSide.LEFT) {
                float calGround = calibrationResult.GetComponent<CalibrationResult>().height.footLeft + this.heightToleranceInCm;
                float calAir = calibrationResult.GetComponent<CalibrationResult>().kneeHeight.kneeLeft - this.heightToleranceInCm;
                float result = (height - calGround) / (calAir - calGround);
                if (result < 0f) return 0f;
                if(result > 1f) return 1f;
                return result;
            } else {
                float calGround = calibrationResult.GetComponent<CalibrationResult>().height.footRight + this.heightToleranceInCm;
                float calAir = calibrationResult.GetComponent<CalibrationResult>().kneeHeight.kneeRight - this.heightToleranceInCm;
                float result = (height - calGround) / (calAir - calGround);
                if(result < 0f) return 0f;
                if(result > 1f) return 1f;
                return result;
            }
        } else {
            throw new System.Exception("Knee-Height not calibrated yet, but getKneePulledUpFactor was called.");
        }
    }

    protected void setMaterialFoot(Material material) {
        if(this.foot == FootSide.LEFT) {
            // Get lower leg
            GameObject lowerLegModel = this.gameObject.transform.FindChild("LowerLeftFoot").gameObject;
            lowerLegModel.GetComponent<Renderer>().material = material;
        } else {
            // Get lower leg
            GameObject lowerLegModel = this.gameObject.transform.FindChild("LowerRightFoot").gameObject;
            lowerLegModel.GetComponent<Renderer>().material = material;
        }
    }

}
