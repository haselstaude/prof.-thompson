﻿using UnityEngine;
using System.Collections;

public class Calibration : MonoBehaviour {

    protected bool doCalibration = false;

    public GameObject calibrationResult;
    protected CalibrationResult results;
    public GameObject controllerLeft;
    public GameObject controllerRight;

    protected bool calibrationHeightDone = false;

    void Start() {
        this.results = this.calibrationResult.GetComponent<CalibrationResult>();
    }

    void Update() {
        if(this.doCalibration) {

            if(this.results.height.isCalibrated == false) {
                // check if calibration is possible
                bool isLeftTriggerPressed = controllerLeft.GetComponent<WandController>().Trigger > 90;
                bool isRightTriggerPressed = controllerRight.GetComponent<WandController>().Trigger > 90;

                if(isLeftTriggerPressed || isRightTriggerPressed) {
                    calibrateHeight();
                    this.results.height.isCalibrated = true;
                }
            } else {
                if(calibrationHeightDone == false) {
                    if(controllerLeft.GetComponent<WandController>().Trigger < 10 && controllerRight.GetComponent<WandController>().Trigger < 10) {
                        this.calibrationHeightDone = true;
                    }
                }
                if(this.results.kneeHeight.isCalibrated == false && this.calibrationHeightDone == true) {
                    // check if calibration is possible
                    bool isLeftTriggerPressed = controllerLeft.GetComponent<WandController>().Trigger > 90;
                    bool isRightTriggerPressed = controllerRight.GetComponent<WandController>().Trigger > 90;

                    if(isLeftTriggerPressed || isRightTriggerPressed) {
                        calibrateKneeHeight();
                        this.results.kneeHeight.isCalibrated = true;
                        this.doCalibration = false;
                    }
                }
            }

        }
    }

    public void startCalibrating() {
        this.doCalibration = true;
    }

    public bool isHeightCalibrated() {
        return this.results.height.isCalibrated;
    }

    public bool isKneeCalibrated() {
        return this.results.kneeHeight.isCalibrated;
    }


    public void calibrateHeight() {
        results.height.footLeft = controllerLeft.transform.localPosition.y;
        results.height.footRight = controllerRight.transform.localPosition.y;
    }

    public void calibrateKneeHeight() {
        // find out which one was calibrated
        bool leftCalibrated = controllerLeft.transform.localPosition.y > controllerRight.transform.localPosition.y;
        if(leftCalibrated) {
            // save left
            results.kneeHeight.kneeLeft = controllerLeft.transform.localPosition.y;
            // emulate right
            float differenceHeightRightToLeft = results.height.footRight - results.height.footLeft;
            results.kneeHeight.kneeRight = results.kneeHeight.kneeLeft + differenceHeightRightToLeft;
        } else {
            // save right
            results.kneeHeight.kneeRight = controllerRight.transform.localPosition.y;
            // emulate left
            float differenceHeightLeftToRight = results.height.footLeft - results.height.footRight;
            results.kneeHeight.kneeLeft = results.kneeHeight.kneeRight + differenceHeightLeftToRight;
        }
    }

    

    public CalibrationResult getResults() {
        return this.results;
    }

}
