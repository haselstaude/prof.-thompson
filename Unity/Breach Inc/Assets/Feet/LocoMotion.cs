﻿using UnityEngine;
using System.Collections;

public class LocoMotion : MonoBehaviour {

    [Header("Head & Feet", order = 0)]
    public GameObject head;
    public GameObject footLeft;
    public GameObject footRight;

    [Header("Parameter", order = 1)]
    public GameObject calibrationResult;
    public float triggerHeight = 0.05f;
    public float triggerTiming = 0.5f;
    public float fadeTiming = 0.5f;
    public float stopDistance = 0.15f;

    protected bool moving = false;
    protected float footLeftHeight = 0f;
    protected float footRightHeight = 0f;

    protected bool leftTriggered = false;
    protected bool rightTriggered = false;

    protected Vector3 leftStartPosition;
    protected Vector3 rightStartPosition;

    protected float fadeFactor = 0f;

	// Update is called once per frame
	void Update () {
        if(this.calibrationResult.GetComponent<CalibrationResult>().height.isCalibrated) {
            this.readControllerHeights();
            this.checkTriggerHeights();
            if(this.bothHeightExceeded() && !moving && this.fadeFactor == 0f) {
                this.saveStartPosition();
                this.moving = true;
            } else if(moving) {
                this.increaseFade();
                this.locoMotion();
                if(this.movementStopped() || this.movedToFar()) {
                    this.moving = false;
                }
            } else if (!moving) {
                this.decreaseFade();
                if(this.fadeFactor > 0f) {
                    this.locoMotion();
                }
            }
        }
	}

    protected void readControllerHeights() {
        this.footLeftHeight = this.footLeft.transform.localPosition.y;
        this.footRightHeight = this.footRight.transform.localPosition.y;
    }

    protected void checkTriggerHeights() {
        //check left foot
        if(!this.leftTriggered) {
            this.leftTriggered = this.footLeftHeight >= triggerHeight + this.calibrationResult.GetComponent<CalibrationResult>().height.footLeft;
            if(this.leftTriggered) {
                Invoke("resetLeftTrigger", triggerTiming);
            }
        }

        //check right foot
        if(!this.rightTriggered) {
            this.rightTriggered = this.footRightHeight >= triggerHeight + this.calibrationResult.GetComponent<CalibrationResult>().height.footRight;
            if(this.rightTriggered) {
                Invoke("resetRightTrigger", triggerTiming);
            }
        }
    }

    protected void resetLeftTrigger() {
        this.leftTriggered = false;
    }

    protected void resetRightTrigger() {
        this.rightTriggered = false;
    }

    protected bool bothHeightExceeded() {
        return this.leftTriggered && this.rightTriggered;
    }

    protected void saveStartPosition() {
        this.leftStartPosition = this.footLeft.transform.localPosition;
        this.leftStartPosition.y = 0f;
        this.rightStartPosition = this.footRight.transform.localPosition;
        this.rightStartPosition.y = 0f;
    }

    protected bool movementStopped() {
        return !this.leftTriggered && !this.rightTriggered;
    }

    protected bool movedToFar() {
        Vector3 leftPose = this.footLeft.transform.localPosition;
        leftPose.y = 0f;
        Vector3 distLeft = leftPose - this.leftStartPosition;
        if (distLeft.magnitude > this.stopDistance) {
            return true;
        }

        Vector3 rightPose = this.footRight.transform.localPosition;
        rightPose.y = 0f;
        Vector3 distRight = rightPose - this.rightStartPosition;
        if(distRight.magnitude > this.stopDistance) {
            return true;
        }

        return false;
    }

    protected void increaseFade() {
        if (this.fadeFactor < 1f) {
            this.fadeFactor += Time.deltaTime / this.fadeTiming;
        } else {
            this.fadeFactor = 1f;
        }
    }

    protected void decreaseFade() {
        if(this.fadeFactor > 0f) {
            this.fadeFactor -= Time.deltaTime / this.fadeTiming;
        } else {
            this.fadeFactor = 0f;
        }
    }

    protected void locoMotion() {
        Vector3 newPosition = this.gameObject.transform.position;
        newPosition.x -= (Time.deltaTime * Mathf.Cos((this.getDirection() + 90f) * Mathf.PI / 180f)) * this.fadeFactor;
        newPosition.z += (Time.deltaTime * Mathf.Sin((this.getDirection() + 90f) * Mathf.PI / 180f)) * this.fadeFactor;
        this.gameObject.transform.position = newPosition;
    }

    protected float getDirection() {
        return this.head.transform.eulerAngles.y;
    }
    
}
