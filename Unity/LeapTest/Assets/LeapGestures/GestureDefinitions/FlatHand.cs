﻿using UnityEngine;
using System.Collections;
using System;

public class FlatHand : ILeapGesture {
    protected float MIN_SINGLE_STRETCH = 0.85f;
    protected float MIN_SUMMARIZED_STRETCH = 3.6f;

    /*
     * This gesture ignores the THUMB.
     * All other fingers must exceed a stretch-factor of 85%.
     * In total all other fingers must exceed a summarized stretch-factor of 3.6f
     */

    public string getTitle() {
        return "Flat Hand";
    }

    public bool isGestured(LeapHand hand) {
        float index_stretch = hand.getFinger(LeapHand.Finger.INDEX).getStretchFactor();
        if(index_stretch < MIN_SINGLE_STRETCH)
            return false;

        float middle_stretch = hand.getFinger(LeapHand.Finger.MIDDLE).getStretchFactor();
        if(middle_stretch < MIN_SINGLE_STRETCH)
            return false;

        float ring_stretch = hand.getFinger(LeapHand.Finger.RING).getStretchFactor();
        if(ring_stretch < MIN_SINGLE_STRETCH)
            return false;

        float pinky_stretch = hand.getFinger(LeapHand.Finger.PINKY).getStretchFactor();
        if(pinky_stretch < MIN_SINGLE_STRETCH)
            return false;

        return index_stretch + middle_stretch + ring_stretch + pinky_stretch >= MIN_SUMMARIZED_STRETCH; 
    }
}
