﻿using UnityEngine;
using System.Collections;
using System;

public class Fist : ILeapGesture {
    protected float MAX_SINGLE_STRETCH = 0.55f;
    protected float MAX_SUMMARIZED_STRETCH = 2.0f;

    /*
     * This gesture ignores the THUMB.
     * All other fingers must not exceed a stretch-factor of 55%.
     * In total all other fingers must not exceed a summarized stretch-factor of 2.0
     */

    public string getTitle() {
        return "Fist";
    }

    public bool isGestured(LeapHand hand) {
        float index_stretch = hand.getFinger(LeapHand.Finger.INDEX).getStretchFactor();
        if(index_stretch > MAX_SINGLE_STRETCH)
            return false;

        float middle_stretch = hand.getFinger(LeapHand.Finger.MIDDLE).getStretchFactor();
        if(middle_stretch > MAX_SINGLE_STRETCH)
            return false;

        float ring_stretch = hand.getFinger(LeapHand.Finger.RING).getStretchFactor();
        if(ring_stretch > MAX_SINGLE_STRETCH)
            return false;

        float pinky_stretch = hand.getFinger(LeapHand.Finger.PINKY).getStretchFactor();
        if(pinky_stretch > MAX_SINGLE_STRETCH)
            return false;

        return index_stretch + middle_stretch + ring_stretch + pinky_stretch <= MAX_SUMMARIZED_STRETCH; 
    }
}
