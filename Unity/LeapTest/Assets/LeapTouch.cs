﻿using UnityEngine;
using System.Collections;
using Leap.Unity;

public class LeapTouch : MonoBehaviour {

    /*
     * Has to be applied on Objects that should be touchable by Leap
     */

    public GameObject leapLeftHand;
    public GameObject leapRightHand;

    public Material materialTouched;

    protected Material materialDefault;
    private Bounds boundsCollider;

    protected bool touchingLeft = false;
    protected bool touchingRight = false;

	
	void Start () {
        this.materialDefault = this.gameObject.GetComponent<Renderer>().material;
        this.boundsCollider = this.gameObject.GetComponent<Collider>().bounds;
	}

    protected virtual void Update() {
        this.touchingLeft = this.isHandTouching(this.leapLeftHand);
        this.touchingRight = this.isHandTouching(this.leapRightHand);
        this.doTouchColoring();
    }


    bool isHandTouching(GameObject hand) {
        bool touching = false;

        // check fingers
        foreach(FingerModel finger in hand.GetComponent<RigidHand>().fingers) {
            touching = this.isFingerTouching(finger.GetTipPosition());
            if (touching)
                return true;
        }

        return false;
    }
    
    bool isFingerTouching(Vector3 finger) {
        bool insideX = (this.boundsCollider.min.x < finger.x) && (finger.x < this.boundsCollider.max.x);
        if(!insideX)
            return false;
        bool insideY = (this.boundsCollider.min.y < finger.y) && (finger.y < this.boundsCollider.max.y);
        if(!insideY)
            return false;
        bool insideZ = (this.boundsCollider.min.z < finger.z) && (finger.z < this.boundsCollider.max.z);
        if(!insideZ)
            return false;
        return true;
    }

    void doTouchColoring() {
        if (this.touchingLeft || this.touchingRight) {
            this.gameObject.GetComponent<Renderer>().material = this.materialTouched;
        } else {
            this.gameObject.GetComponent<Renderer>().material = this.materialDefault;
        }
    }
}
