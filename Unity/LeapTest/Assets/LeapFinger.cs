﻿using UnityEngine;
using System.Collections;

public class LeapFinger : MonoBehaviour {

    /*
     * Has to be applied on each finger below RigidRoundHand_L and RigidRoundHand_R
     */

    protected GameObject[] bones;

    void Start() {
        this.linkBones();
    }


    /// <summary>
    /// Returns the "proximal bone" - the bone closest to the palm
    /// </summary>
    /// <returns>Proximal bone - the bone closest to the palm</returns>
    public GameObject getProximalBone() {
        return this.bones[0];
    }

    /// <summary>
    /// Returns the "middle bone"
    /// </summary>
    /// <returns>Middle bone</returns>
    public GameObject getMiddleBone() {
        return this.bones[1];
    }

    /// <summary>
    /// Returns the "distal bone" - represents the finger tip
    /// </summary>
    /// <returns>Distal bone - represents the finger tip</returns>
    public GameObject getDistalBone() {
        return this.bones[2];
    }

    /// <summary>
    /// Returns how much a finger is stretched out
    /// 1f would be a perfect stretch
    /// 0f would be a perfect close
    /// </summary>
    /// <returns>Stretch-factor (1f would be a perfect stretch - 0f a perfect close)</returns>
    public float getStretchFactor() {
        // getting required finger bones angles
        Vector3 angleProx = this.getProximalBone().transform.localEulerAngles;
        Vector3 angleDist = this.getDistalBone().transform.localEulerAngles;

        // check if there are angles which are around the 0 degree layer
        if (Mathf.Abs(angleProx.x - angleDist.x) > 250f) {
            if (angleProx.x < angleDist.x) {
                angleProx.x += 360f;
            } else {
                angleDist.x += 360f;
            }
        }
        if(Mathf.Abs(angleProx.y - angleDist.y) > 250f) {
            if(angleProx.y < angleDist.y) {
                angleProx.y += 360f;
            } else {
                angleDist.y += 360f;
            }
        }
        if(Mathf.Abs(angleProx.z - angleDist.z) > 250f) {
            if(angleProx.z < angleDist.z) {
                angleProx.z += 360f;
            } else {
                angleDist.z += 360f;
            }
        }

        // angle-difference of proximal and distal bone
        // 0° -> 1f ; 180° -> 0f
        Vector3 angleDiff = angleProx - angleDist;
        float angleDiffValue = angleDiff.magnitude;
        return Mathf.Max(0f, 1f - angleDiffValue / 360f);
    }


    /// <summary>
    /// Gets the three bones of a finger and adds them to the bones-array
    /// Array is sorted beginning by the palm - so the last element is the tip of the finger
    /// </summary>
    protected void linkBones() {
        this.bones = new GameObject[3];
        for(int arrayIdx = 0; arrayIdx < 3; arrayIdx++) {
            this.bones[arrayIdx] = this.gameObject.transform.Find("bone" + (arrayIdx + 1)).gameObject;
        }
    }
}
