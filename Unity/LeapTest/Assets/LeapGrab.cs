﻿using UnityEngine;
using System.Collections;

public class LeapGrab : LeapTouch {

    public Material materialGrabbed;

    protected bool grabbingLeft = false;
    protected bool grabbingRight = false;

    protected Vector3 rightGrabStartPosHand;
    protected Vector3 rightGrabStartRotHand;
    protected Vector3 rightGrabStartPosObject;
    protected Vector3 rightGrabStartRotObject;

    protected Vector3 leftGrabStartPosHand;
    protected Vector3 leftGrabStartRotHand;
    protected Vector3 leftGrabStartPosObject;
    protected Vector3 leftGrabStartRotObject;


    protected override void Update() {
        base.Update();

        if (!this.grabbingLeft && !this.grabbingRight) {
            // not grabbed yet

            // check right hand first
            if (this.touchingRight) {
                if (this.leapRightHand.GetComponent<GestureDetector>().isGesturingFist()) {
                    // touching and fist -> grabbing
                    this.grabbingRight = true;
                    this.saveDataForGrabStartRight();
                }
            }

            if(!this.grabbingRight) {
                // check left hand
                if(this.touchingLeft) {
                    if(this.leapLeftHand.GetComponent<GestureDetector>().isGesturingFist()) {
                        // touching and fist -> grabbing
                        this.grabbingLeft = true;
                        this.saveDataForGrabStartLeft();
                    }
                }
            }
        } else {
            // already grabbing

            if (this.grabbingRight) {
                // grabbing with right
                this.moveObjectLinkedToRightHand();

                // check un-grab
                if(this.leapRightHand.GetComponent<GestureDetector>().isGesturingFlatHand()) {
                    // touching and flat hand -> un-grabbing
                    this.grabbingRight = false;
                }
            } else {
                // grabbing with left
                this.moveObjectLinkedToLeftHand();

                // check un-grab
                if(this.leapLeftHand.GetComponent<GestureDetector>().isGesturingFlatHand()) {
                    // touching and flat hand -> un-grabbing
                    this.grabbingLeft = false;
                }
            }
        }

        this.doGrabColoring();
    }

    protected void saveDataForGrabStartRight() {
        Transform palm = this.leapRightHand.GetComponent<LeapHand>().getPalmTransform();
        this.rightGrabStartPosHand = palm.position;
        this.rightGrabStartRotHand = palm.eulerAngles;

        Transform obj = this.gameObject.transform;
        this.rightGrabStartPosObject = obj.position;
        this.rightGrabStartRotObject = obj.eulerAngles;
    }

    protected void saveDataForGrabStartLeft() {
        Transform palm = this.leapLeftHand.GetComponent<LeapHand>().getPalmTransform();
        this.leftGrabStartPosHand = palm.position;
        this.leftGrabStartRotHand = palm.eulerAngles;

        Transform obj = this.gameObject.transform;
        this.leftGrabStartPosObject = obj.position;
        this.leftGrabStartRotObject = obj.eulerAngles;
    }

    protected void moveObjectLinkedToRightHand() {
        Vector3 posDiff = this.leapRightHand.GetComponent<LeapHand>().getPalmTransform().position - this.rightGrabStartPosHand;
        Vector3 rotDiff = this.leapRightHand.GetComponent<LeapHand>().getPalmTransform().eulerAngles - this.rightGrabStartRotHand;

        this.gameObject.transform.position = this.rightGrabStartPosObject + posDiff;
        this.gameObject.transform.eulerAngles = this.rightGrabStartRotObject - rotDiff;
    }

    protected void moveObjectLinkedToLeftHand() {
        Vector3 posDiff = this.leapLeftHand.GetComponent<LeapHand>().getPalmTransform().position - this.leftGrabStartPosHand;
        Vector3 rotDiff = this.leapLeftHand.GetComponent<LeapHand>().getPalmTransform().eulerAngles - this.leftGrabStartRotHand;

        this.gameObject.transform.position = this.leftGrabStartPosObject + posDiff;
        this.gameObject.transform.eulerAngles = this.leftGrabStartRotObject - rotDiff;
    }

    protected void doGrabColoring() {
        if(this.grabbingLeft || this.grabbingRight) {
            this.gameObject.GetComponent<Renderer>().material = this.materialGrabbed;
        } else {
            this.gameObject.GetComponent<Renderer>().material = this.materialDefault;
        }
    }
}
