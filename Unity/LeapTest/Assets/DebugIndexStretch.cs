﻿using UnityEngine;
using System.Collections;

public class DebugIndexStretch : MonoBehaviour {

	// Update is called once per frame
	void Update () {
        Debug.Log(this.gameObject.GetComponent<LeapHand>().getFinger(LeapHand.Finger.INDEX).getStretchFactor());
	}
}
