﻿using UnityEngine;
using System.Collections;

public class LeapHand : MonoBehaviour {

    /*
     * Has to be applied on RigidRoundHand_L and RigidRoundHand_R
     */

    public enum Finger {
        THUMB = 0,
        INDEX = 1,
        MIDDLE = 2,
        RING = 3,
        PINKY = 4
    }

    protected LeapFinger[] fingers;
    protected GameObject palm;


	void Start () {
        this.linkFingers();
        this.palm = this.gameObject.transform.Find("palm").gameObject;
    }


    /// <summary>
    /// Returns a finger
    /// </summary>
    /// <param name="finger">Which finger?</param>
    /// <returns>Finger</returns>
    public LeapFinger getFinger(Finger finger) {
        return this.fingers[(int)finger];
    }

    /// <summary>
    /// Checks if hand does a given gesture
    /// </summary>
    /// <param name="gesture">Gesture</param>
    /// <returns>Hand gesturing given gesture?</returns>
    public bool isGesturing(ILeapGesture gesture) {
        return gesture.isGestured(this);
    }

    /// <summary>
    /// Returns the palm transform (is used as reference movement for grabbed objects)
    /// </summary>
    /// <returns>Palm Transform</returns>
    public Transform getPalmTransform() {
        return this.palm.transform;
    }


    /// <summary>
    /// Gets the finger of a hand and adds them to the fingers-array
    /// Array is sorted from THUMB to PINKY
    /// </summary>
    protected void linkFingers() {
        this.fingers = new LeapFinger[5];

        this.fingers[0] = this.gameObject.transform.Find("thumb").gameObject.GetComponent<LeapFinger>();
        if(fingers[0] == null)
            throw new MissingComponentException("Thumb of " + this.gameObject.name + " is missing LeapFinger-Script!");

        this.fingers[1] = this.gameObject.transform.Find("index").gameObject.GetComponent<LeapFinger>();
        if(fingers[1] == null)
            throw new MissingComponentException("Index of " + this.gameObject.name + " is missing LeapFinger-Script!");

        this.fingers[2] = this.gameObject.transform.Find("middle").gameObject.GetComponent<LeapFinger>();
        if(fingers[2] == null)
            throw new MissingComponentException("Middle of " + this.gameObject.name + " is missing LeapFinger-Script!");

        this.fingers[3] = this.gameObject.transform.Find("ring").gameObject.GetComponent<LeapFinger>();
        if(fingers[3] == null)
            throw new MissingComponentException("Ring of " + this.gameObject.name + " is missing LeapFinger-Script!");

        this.fingers[4] = this.gameObject.transform.Find("pinky").gameObject.GetComponent<LeapFinger>();
        if(fingers[4] == null)
            throw new MissingComponentException("Pinky of " + this.gameObject.name + " is missing LeapFinger-Script!");
    }
}
