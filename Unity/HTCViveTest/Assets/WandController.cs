﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WandController : MonoBehaviour {

    // Value Display for Unity
    public bool Grip;
    public bool Menu;
    public Vector2 Touchpad_Touch;
    public bool Touchpad_Press;
    public byte Trigger;

    // Tracked object
    private SteamVR_TrackedObject trackedObj;

    // Index of the tracked object
    private int index {
        get {
            return (int) this.trackedObj.index;
        }
    }

    // Controller
    private SteamVR_Controller.Device controller {
        get {
            return SteamVR_Controller.Input((int) this.trackedObj.index);
        }
    }
    
    // Button-List
    public static class Button {
        public static Valve.VR.EVRButtonId Grip = Valve.VR.EVRButtonId.k_EButton_Grip;
        public static Valve.VR.EVRButtonId Menu = Valve.VR.EVRButtonId.k_EButton_ApplicationMenu;
        /* NOT WORKING (BUT NOT NEEDED AS WELL) public static Valve.VR.EVRButtonId System = Valve.VR.EVRButtonId.k_EButton_System; */
        public static Valve.VR.EVRButtonId Touchpad = Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad;
        public static Valve.VR.EVRButtonId Trigger = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
    }

    // Use this for initialization
    void Start () {
        this.trackedObj = GetComponent<SteamVR_TrackedObject>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (controller != null) {
            #region Menu-Press-Check
            if ( this.controller.GetPressDown(WandController.Button.Menu) ) {
                Debug.Log("Device #" + this.index + ": Menu-Button pressed");
            }
            if ( this.controller.GetPressUp(WandController.Button.Menu) ) {
                Debug.Log("Device #" + this.index + ": Menu-Button released");
            }
            this.Menu = this.controller.GetPress(WandController.Button.Menu);
            #endregion
            /* NOT WORKING (BUT NOT NEEDED AS WELL)
            #region System-Press-Check
            if ( this.controller.GetPressDown(WandController.Button.System) ) {
                Debug.Log("Device #" + this.index + ": System-Button pressed");
            }
            if ( this.controller.GetPressUp(WandController.Button.System) ) {
                Debug.Log("Device #" + this.index + ": System-Button released");
            }
            #endregion
            */
            #region Grip-Press-Check
            if ( this.controller.GetPressDown(WandController.Button.Grip) ) {
                Debug.Log("Device #" + this.index + ": Grip-Button pressed");
            }
            if ( this.controller.GetPressUp(WandController.Button.Grip) ) {
                Debug.Log("Device #" + this.index + ": Grip-Button released");
            }
            this.Grip = this.controller.GetPress(WandController.Button.Grip);
            #endregion
            #region Touchpad-Press-Check
            if ( this.controller.GetPressDown(WandController.Button.Touchpad) ) {
                Debug.Log("Device #" + this.index + ": Touchpad pressed");
            }
            if ( this.controller.GetPressUp(WandController.Button.Touchpad) ) {
                Debug.Log("Device #" + this.index + ": Touchpad released");
            }
            this.Touchpad_Press = this.controller.GetPress(WandController.Button.Touchpad);
            #endregion
            /* NOT NEEDED BECAUSE ANALOG TRIGGER VALUE
            #region Trigger-Press-Check
            if ( this.controller.GetPressDown(WandController.Button.Trigger) ) {
                Debug.Log("Device #" + this.index + ": Trigger pulled");
            }
            if ( this.controller.GetPressUp(WandController.Button.Trigger) ) {
                Debug.Log("Device #" + this.index + ": Trigger released");
            }
            #endregion
            */
            this.Touchpad_Touch = this.controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0);
            this.Trigger = (byte) (this.controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis1).x * 100);
        } else {
            Debug.Log("Controller lost connection!");
            return;
        }
	}
}
