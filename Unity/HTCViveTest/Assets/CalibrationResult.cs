﻿using UnityEngine;
using System.Collections;

public class CalibrationResult : MonoBehaviour {

    #region class definitions
    /// <summary>
    /// height of controllers then player stands
    /// </summary>
    public class Height {
        public float footLeft;
        public float footRight;
        public bool isCalibrated = false;
    }

    /// <summary>
    /// height of controllers then knee has 90° to hip
    /// </summary>
    public class KneeHeight {
        public float kneeLeft;
        public float kneeRight;
        public bool isCalibrated = false;
    }

    /// <summary>
    /// angle of controllers (Y)
    /// </summary>
    public class Direction {
        public float directionLeft;
        public float directionRight;
        public bool isCalibrated = false;
    }
    #endregion

    public Height height;
    public KneeHeight kneeHeight;
    public Direction direction;

	public CalibrationResult() {
        this.height = new Height();
        this.kneeHeight = new KneeHeight();
        this.direction = new Direction();
    }
}
