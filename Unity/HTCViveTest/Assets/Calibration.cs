﻿using UnityEngine;
using System.Collections;

public class Calibration : MonoBehaviour {

    public GameObject calibrationResult;
    protected CalibrationResult results;
    public GameObject controllerLeft;
    public GameObject controllerRight;

    protected bool calibrationHeightDone = false;

    void Start() {
        this.results = this.calibrationResult.GetComponent<CalibrationResult>();
    }

    void Update() {
        if(this.results.height.isCalibrated == false) {
            // check if calibration is possible
            bool isLeftTriggerPressed = controllerLeft.GetComponent<WandController>().Trigger > 90;
            bool isRightTriggerPressed = controllerRight.GetComponent<WandController>().Trigger > 90;

            if(isLeftTriggerPressed || isRightTriggerPressed) {
                calibrateHeight();
                this.results.height.isCalibrated = true;
                Debug.Log("Y left: " + this.results.height.footLeft);
                Debug.Log("Y Right: " + this.results.height.footRight);
            }
        } else {
            if(calibrationHeightDone == false) {
                if(controllerLeft.GetComponent<WandController>().Trigger < 10 && controllerRight.GetComponent<WandController>().Trigger < 10) {
                    this.calibrationHeightDone = true;
                }
            }
            if(this.results.kneeHeight.isCalibrated == false && this.calibrationHeightDone == true) {
                // check if calibration is possible
                bool isLeftTriggerPressed = controllerLeft.GetComponent<WandController>().Trigger > 90;
                bool isRightTriggerPressed = controllerRight.GetComponent<WandController>().Trigger > 90;

                if(isLeftTriggerPressed || isRightTriggerPressed) {
                    calibrateKneeHeight();
                    this.results.kneeHeight.isCalibrated = true;
                    Debug.Log("Y Knee left: " + this.results.kneeHeight.kneeLeft);
                    Debug.Log("Y Knee Right: " + this.results.kneeHeight.kneeRight);
                }
            }
        }


    }

	public void calibrateDirection() {
        // TODO
    }

    public void calibrateHeight() {
        results.height.footLeft = controllerLeft.transform.position.y;
        results.height.footRight = controllerRight.transform.position.y;
    }

    public void calibrateKneeHeight() {
        results.kneeHeight.kneeLeft = controllerLeft.transform.position.y;
        results.kneeHeight.kneeRight = controllerRight.transform.position.y;
    }

    public CalibrationResult getResults() {
        return this.results;
    }

}
