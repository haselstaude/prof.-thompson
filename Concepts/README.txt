Please sort in the concepts regarding to the category
Please save every concept as DOC (not DOCX)
One Concept per file

If you want to comment a concept write in YOUR COLOR into the file (do not erase stuff)
Noni   RED
Staudi BLUE
Lukas  PURPLE

Big Puzzles		Puzzles which may take a while (i.e. have to combine objects, need multiples objects, ...)
Doorlock-Puzzles	Puzzles which need to be solved to open a door
Mechanisms		Mechanisms which affects the world(s) (i.e. moving platforms, minigames to help the controller-player, ...)
Rooms			Special rooms (i.e. labyrinths, portal movement, ...)